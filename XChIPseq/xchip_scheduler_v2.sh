#!/bin/bash -l 
#SBATCH --partition=panda 
#SBATCH --cpus-per-task=1 
#SBATCH --ntasks=1
#SBATCH --job-name=CHIP-ATAC
#SBATCH --time=72:00:00  
#SBATCH --mem=8G
#SBATCH --mail-user=hssgenomics@gmail.com 
#SBATCH --mail-type=END,FAIL

# set -x

# ///////////////////////////////////////////////////////////////////////////
#
# qsub scripts are executed in the CURRENT WORKING DIRECTORY
# this means that all scripts in the CWD directory can
# be called using ./myscript.sh
#
# This pipeline follows a heirarchical rationale that should allow for modular
# development/improvements over time with minimal effort.
#
# xchip_scheduler.sh : builds bash scripts from scripts and templates
# |
# |-- slurmSample.template : performs all sample related processing
# |   |
# |   |-- copy to tmpdir
# |   |-- read filtering
# |   |-- qc
# |   |-- align
# |   |-- alignment filtering
# |   |-- copy back to cwd 
# |
# |-- slurmGroup.template : performs all group level processing
# |   |
# |   |-- call peaks
# |   |-- make cutsite matrix
# |   |-- footprinting 
# |  
# |-- slurmExperiment.template : perform all experiment level processing
#     | 
#     |-- aggregate stats (multiqc)
#     |-- 
#
# The scripts called by each of the above slurm scripts are located
# in similarly structured `scripts` directory (template.sh is useful)
# 
# scripts
# ├── experiment
# │   └── runmultiqc.sh
# ├── group
# │   └── callpeaks.sh
# ├── sample
# │   ├── cpgenome.sh
# │   ├── runalign.sh
# │   ├── samplecleanup.sh
# └── template.sh
#
#
# NOTES for further development:
# 
# Replacements within template are formed as below (with the exception of slurm header)
# 1. they start and end with 2 underscore
# 2. they start with lowercase do followed by camel case descriptive
# ex. __doSomeThing__ / __doRunAlign__
#
# Function calls are stored as variables following the convention below
# 1. lowecase first descriptive word (usually abbreviated)
# 2. all uppercase second descriptive word
# ex. runALIGN / callPEAKS
#
# non-function call variables are ALWAYS uppercase
# ex. MODE / GROUP / ID
# variables within functions should be scoped with `local` and 
# be all lower-case to avoid possible conflicts or failure in scoping
# ex. local stat / local jobid
#
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

###########
# FUNCTIONS
###########

# monitor: given an array of job IDs, monitor their progress
function monitor {
  # get the parameters from command line
  local delay="$1"
  shift
  local jobarr=("$@")
  # set status and start looping
  local stat="INCOMPLETE"
  while [ "$stat" = "INCOMPLETE" ]; do
    if [ ${#jobarr[@]} -gt 0 ]; then
      local checkstat=$(sacct --format=state -n -P -j "${jobarr[0]}" | sort | uniq)
      if [ "$checkstat" = "COMPLETED" ]; then
        local jobarr=("${jobarr[@]:1}")
        continue
      elif [[ $checkstat =~ TIMEOUT|FAILED|CANCELLED ]]; then
        local stat="FAILED"
        exit 1 # throw non-zero exit instead of continue
      fi
    else
      local stat="COMPLETE"
      continue
    fi
    sleep $delay
  done
}

function call {
  # local scopes variable to within this function
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

# General usage statement
function usage { 
  echo "Usage: sbatch xchip_scheduler.sh in the directory where you want the results output.
  -m mode -- select a mode for optimizing parameters. choices: atac, chip, cutNrun
  -i path to the project directory containing sample_ directories with fastq files (full path).
  -o destination directory -- corresponds to the project directory in the analysis branch.
  -n name -- sample name that matches all samples (e.g. Sample_KL).
  -g genome -- hg38, mm10 or rn6.
  -a annotation -- if hg38 or mm10 then gencode, otherwise ensembl
  -r groups -- a space delimited text file assigning each sample to a group  
  -f filter -- does the fastq file contain reads that failed illumina chastity fitler?
  -l aligner -- which aligner to use. Default = bt2
  -p peak caller -- either genrich or macs2. Default = genrich
  -e email -- defaults to hssgenomics.at.gmail.dot.com
  -h help -- prints this usage message." 
  exit 1
}

# Variables
MODE=""     # no default...must select a mode
IN=""       # No default, script fails if full path not assigned
OUT=""      # Destination directory (should be corresponding analysis dir)
NAME=""     # The name common to all samples in the experiment
GENOME=""   # The genome build that that the samples were aligned to
ANNO=""     # default should probably be gencode...
GROUP=""    # grouping, no default
ALIGN="bt2" # defualt aligner is STAR
PF="FALSE"  # does fastq include "Y" reads?
PEAK="genrich" # default to genrich (until macs2 dies in hell)
EMAIL="hssgenomics@gmail.com"

#
# Parse command-line arguments
#
while getopts :m:i:o:n:g:a:r:f:l:p:e:h: opt; do
  case $opt in
    m)
      MODE=$OPTARG
      ;;
    i)
      IN=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    n)
      NAME=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    a)
      ANNO=$OPTARG
      ;;
    r)
      GROUP=$OPTARG
      ;;
    f)
      PF=$OPTARG
      ;;
    l)
      ALIGN=$OPTARG
      ;;
    p)
      PEAK=$OPTARG
      ;;
    e)
      EMAIL=$OPTARG
      ;;
    h)
      usage
      ;;
    *) 
      usage
      ;;
  esac
done

# set common variables (do not change with samples)
GENOMELOC=/athena/hssgenomics/scratch/genomes_2/$GENOME/Genome/$ALIGN/
ANNOLOC=/athena/hssgenomics/scratch/genomes_2/$GENOME/Annotation/${ANNO}.gtf
BLACKLIST=/athena/hssgenomics/scratch/genomes_2/$GENOME/Annotation/blacklist.bed

# Check validity of inputs
if [ ! -d "$IN" ] || [ "$IN" == "" ]; then
  printf "Invalid path to fastq directory. \n"
  usage
elif [ "$(find "$IN" -maxdepth 1 -name "$NAME*" 2>/dev/null | wc -l)" == 0 ] || [ "$NAME" == "" ]; then
  printf "Invalid name argument. \n"
  usage
elif [ "$NAME" == "" ]; then
  printf "Invalid Sample ID. \n"
  usage
elif [[ ! $GENOME =~ hg38|mm10|rn6 ]] || [ ! -d "$GENOMELOC" ]; then
  printf "Invalid genome selected (only mm10, rn6, and hg38 are currently supported). \n"
  usage
elif [ "$ANNO" == "" ] || [ ! -f "$ANNOLOC" ]; then
  printf "Annotation not a file or is not specified. \n"
  usage
elif [[ ! $ALIGN =~ bt2|bwa ]]; then
  printf "Incorrect aligner specified, only hisat2 and STAR supported. \n"
  usage
elif [[ ! $MODE =~ atac|chip|cutNrun ]]; then
  printf "Incompatible mode selected. \n"
  usage
elif [[ ! $PEAK =~ genrich|macs2 ]]; then
  printf "Incompatible mode selected. \n"
  usage
elif [ ! -f "$GROUP" ]; then
  printf "%s is not a file. Create a space separated group file. \n" $GROUP
  usage
fi

###
##
# Here we run analyses for each sample (e.g. pre-processing)
##
###
THREADS=6 # these are testing parameters...might need adjusting
MEMFREE=16 # ^^^

# store the original parameters for later use
call "$0" "$@" > ${OUT}/rerun_pipeline.sh

# find the directories that match $NAME in the $IN directory
mapfile -t SAMPLEDIRS < <(find "$IN" -type d -name "${NAME}*")
# for each directory found:
for (( i=0; i<${#SAMPLEDIRS[*]}; i++ )); do
  # get the sample name
  SAMPLENAME=$(basename "${SAMPLEDIRS[$i]}")
  # generate the output directories
  mkdir -p ${OUT}/${SAMPLENAME}
  # create the output name
  ID=${SAMPLENAME}_${ALIGN}_${PEAK} # anything starting with $ID is part of the final output
  # single and paired end aware copying data to -o dir
  cpFASTQ="./scripts/sample/cpfastq.sh -s $SAMPLENAME -i $ID -d ${SAMPLEDIRS[$i]} -o \$TMPDIR"
  # run pass filter filtering ... e.g. illumina chastity fail
  pfFILTER="./scripts/sample/pfilter.sh -f $PF -s $SAMPLENAME -i $ID -d \$TMPDIR -o \$TMPDIR"
  # fastp now runs from the qcfastp.sh script, modify that script as necessary
  qcFASTP="./scripts/sample/qcfastp.sh -t $THREADS -s $SAMPLENAME -i $ID -d \$TMPDIR -o \$TMPDIR"
  # split data into 15M read fastq files and write them to -o $OUT
  # they need to be written somewhere visible by new jobs (e.g. not $TMPDIR)
  fqSPLIT="./scripts/sample/fqsplit.sh -r 100000000 -s $SAMPLENAME -i $ID -d \$TMPDIR -o ${OUT}/${SAMPLENAME}"
  cpLOGS="cp \$TMPDIR/${ID}* ${OUT}/${SAMPLENAME}/"
  # build the fastq copying and cleaning command
  sed '
s@__JOB_NAME__@'"cleanFQ_${SAMPLENAME}"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s/__EMAIL__/'"$EMAIL"'/
s@__runGenericCommand__@'"$cpFASTQ \&\& $pfFILTER \&\& $qcFASTP \&\& $fqSPLIT \&\& $cpLOGS"'@
' header.tmp > cleanFQ_${ID}.sh  
  # sumbit the script via sbatch
  if [ "$CLEANJOBIDS" == "" ]; then
    CLEANJOBIDS=$(sbatch cleanFQ_${ID}.sh | cut -d' ' -f4) # Submitted batch job ->JOBID<-
  else
    CLEANJOBIDS=$CLEANJOBIDS:$(sbatch cleanFQ_${ID}.sh | cut -d' ' -f4)
  fi
done

# need to wait for cleaning jobs to finish (otherwise split files don't exist...)
CHECK=($(echo $CLEANJOBIDS | tr ':' ' '))
# this takes about 5min for small and 30min for larger dataset
monitor 300 "${CHECK[@]}"

# here to ensure that all files align in reasonable time 
# we spin a job for each split fastq file and merge them later

# TODO: consider abstracting away some of this mess...
THREADS=32 # these are testing parameters...might need adjusting
MEMFREE=64 # ^^^
# unset previous SAMPLEDIRS (since it was found in the $IN dir initially)
unset SAMPLEDIRS
# find the new sample dirs in the $OUT dir
mapfile -t SAMPLEDIRS < <(find "$OUT" -type d -name "${NAME}*")
# for each dir containing split fastq files
for (( i=0; i<${#SAMPLEDIRS[*]}; i++ )); do
  # get current sample name
  SAMPLENAME=$(basename "${SAMPLEDIRS[$i]}")
  # set the current sample "ID"
  ID=${SAMPLENAME}_${ALIGN}_${PEAK}
  # check if the current sample is PE or SE 
  ISPE=$(ls ${SAMPLEDIRS[$i]}/*R2*.fastq 2>/dev/null | wc -l) 
  if [ "$ISPE" -gt 0 ]; then
    # find all the split fastq files in this dir
    READONEFQ=($(readlink -f $(find $OUT -name "${SAMPLENAME}.R1.merged.qc.a*.fastq" | sort)))
    READTWOFQ=($(readlink -f $(find $OUT -name "${SAMPLENAME}.R2.merged.qc.a*.fastq" | sort)))
    # for each fastq in the split
    for (( j=0; j<${#READONEFQ[*]}; j++ )); do
      # build the appropriate runALIGN command
      if [ "$ALIGN" == "bt2" ]; then
        runALIGN="bowtie2 --sensitive-local -q -p $THREADS -x ${GENOMELOC}/${GENOME} \
                  --no-unal --no-mixed --no-discordant --dovetail --phred33 -I 40 -X 700 \
                  -1 ${READONEFQ[$j]} -2 ${READTWOFQ[$j]} \
                  2>${OUT}/${SAMPLENAME}/${SAMPLENAME}.${j}.bt2.log \
                  > ${OUT}/${SAMPLENAME}/${SAMPLENAME}.${j}.bt2.sam"
      elif [ "$ALIGN" == "bwa" ]; then
        runALIGN="bwa mem -a -t $THREADS ${GENOMELOC}/genome \
                  ${READONEFQ[$j]} ${READTWOFQ[$j]} \
                  2>${OUT}/${SAMPLENAME}/${SAMPLENAME}.${j}.bwa.log \
                  > ${OUT}/${SAMPLENAME}/${SAMPLENAME}.${j}.bwa.sam"
      fi
      # build and submit the script.
      sed '
s@__JOB_NAME__@'"alignFQ_${j}_${ID}"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s/__EMAIL__/'"$EMAIL"'/
s@__runGenericCommand__@'"$runALIGN"'@
' header.tmp > alignFQ_${ID}_${j}.sh  
      # sumbit the script via sbatch...
      # we check in the while loop above if the jobs complete ok. doublechecked here
      if [ "$ALIGNJOBIDS" == "" ]; then
        ALIGNJOBIDS=$(sbatch --dependency=afterok:${CLEANJOBIDS} \
                      alignFQ_${ID}_${j}.sh | cut -d' ' -f4)
      else
        ALIGNJOBIDS=$ALIGNJOBIDS:$(sbatch --dependency=afterok:${CLEANJOBIDS} \
                                   alignFQ_${ID}_${j}.sh | cut -d' ' -f4)
      fi
    done
  else # otherwise if it is single end dataset
    # find all the split fastq files
    READONEFQ=($(readlink -f $(find $OUT -name "${SAMPLENAME}.merged.qc.a*.fastq" | sort))) 
    # for each split fastq file
    for (( j=0; j<${#READONEFQ[*]}; j++ )); do
      # build proper command
      if [ "$ALIGN" == "bt2" ]; then
        runALIGN="bowtie2 --sensitive-local -q -p $THREADS -x ${GENOMELOC}/${GENOME} \
                  --no-unal --phred33 -U ${READONEFQ[$j]} \
                  2>${OUT}/${SAMPLENAME}/${SAMPLENAME}.${j}.bt2.log \
                  > ${OUT}/${SAMPLENAME}/${SAMPLENAME}.${j}.bt2.sam"
      elif [ "$ALIGN" == "bwa" ]; then
        runALIGN="bwa mem -a -t $THREADS ${GENOMELOC}/genome ${READONEFQ[$j]} \
                  2>${OUT}/${SAMPLENAME}/${SAMPLENAME}.${j}.bwa.log \
                  > ${OUT}/${SAMPLENAME}/${SAMPLENAME}.${j}.bwa.sam"
      fi
      sed '
s@__JOB_NAME__@'"alignFQ_${j}_${ID}"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s/__EMAIL__/'"$EMAIL"'/
s@__runGenericCommand__@'"$runALIGN"'@
' header.tmp > alignFQ_${ID}_${j}.sh  
      # sumbit the script via sbatch
      if [ "$ALIGNJOBIDS" == "" ]; then
        ALIGNJOBIDS=$(sbatch --dependency=afterok:${CLEANJOBIDS} \
                      alignFQ_${ID}_${j}.sh | cut -d' ' -f4)
      else
        ALIGNJOBIDS=$ALIGNJOBIDS:$(sbatch --dependency=afterok:${CLEANJOBIDS} \
                                   alignFQ_${ID}_${j}.sh | cut -d' ' -f4)
      fi
    done
  fi
done

## hmmm maybe here we can actually go back to submitting jobs with dependencies and
## exiting in order to avoid 24hr limit

unset CHECK # make sure CHECK is empty
CHECK=($(echo $ALIGNJOBIDS | tr ':' ' '))
# TODO update eta here?
# no reason to expect this to be done in less than 15min
monitor 900 "${CHECK[@]}"

## SAMPLEDIRS have not changed from above. we re-use here.
#for (( i=0; i<${#SAMPLEDIRS[*]}; i++ )); do
#  # get current sample name
#  SAMPLENAME=$(basename "${SAMPLEDIRS[$i]}")
#  # set the current sample "ID"
#  ID=${SAMPLENAME}_${ALIGN}_${PEAK}
#  # merge the alignment files and sort
#  ISPE=$(ls ${SAMPLEDIRS[$i]}/*R2*.fastq 2>/dev/null | wc -l) 
#  if [ "$ISPE" -gt 0 ]; then
#    ISPE="pe" # set PE for this sample
#  else 
#    ISPE="se"
#  fi
#  # -p here is actually the tmpdir for samtools intermediate files
#  samMERGE="./scripts/sample/sammerge.sh -e $ISPE -t $THREADS -s $SAMPLENAME -i $ID \
#            -p \$TMPDIR -d ${OUT}/${SAMPLENAME} -o ${OUT}/${SAMPLENAME}"
#  # Filter out mitochondrial reads...
#  filterMC="./scripts/sample/filtermc.sh -t $THREADS -s $SAMPLENAME -i $ID -d $OUT/${SAMPLENAME} -o $OUT/${SAMPLENAME}"
#  # remove reads aligning to blacklisted regions
#  rmBLACK="./scripts/sample/rmblack.sh -b $BLACKLIST -s $SAMPLENAME -i $ID -d $OUT/${SAMPLENAME} -o $OUT/${SAMPLENAME}"
#  # create a final index
#  # generate some useful stats
#  samSTAT="./scripts/sample/samstat.sh -t $THREADS -s $SAMPLENAME -i $ID -p \$TMPDIR \
#           -d $OUT/${SAMPLENAME} -o $OUT/${SAMPLENAME}"          
#  # collect/clean after sample processing. this moves data from $TMPDIR to $OUT
#  smplCLEAN="./scripts/sample/samplecleanup.sh -s $SAMPLENAME -i $ID \
#             -d $OUT/${SAMPLENAME} -o $OUT/${ID}"
#  # build each sbatch script and submit
#  # note that emails have an @ in them...
#  sed '
#s@__JOB_NAME__@'"$SAMPLENAME"'@
#s@__THREADS__@'"$THREADS"'@
#s@__MEMFREE__@'"$MEMFREE"'@
#s/__EMAIL__/'"$EMAIL"'/
#s@__runGenericCommand__@'"$filterMC \&\& $rmBLACK \&\& $samSTAT \&\& $smplCLEAN"'@
#' header.tmp > filterBAM_${ID}.sh  
#  # sumbit the script via sbatch
#  if [ "$JOBIDS" == "" ]; then
#    FILTERJOBIDS=$(sbatch --dependency=afterok:${ALIGNJOBIDS} \
#                   filterBAM_${ID}.sh | cut -d' ' -f4)
#  else
#    FILTERJOBIDS=$FILTERJOBIDS:$(sbatch --dependency=afterok${ALIGNJOBIDS} \
#                                 filterBAM_${ID}.sh | cut -d' ' -f4)
#  fi
#done

####
###
## Now we can run any analysis with groups of data (e.g. replicates)
###
####

#### NOTE this is where the MODE parameter is used, up till now processing is identical
#
## collect groups of unique data from grouping table
#GRPS=($(cut -d' ' -f 2 "$GROUP" | sort | uniq)) 
## for each group of samples
#for (( j=0; j<${#GRPS[@]}; j++ )); do 
#  # attempting to maintain symetry to the sample portion of the pipeline
#  GROUPNAME=${GRPS[$j]}
#  # we overwrite ID here (bad idea?)
#  ID=${GROUPNAME}_${PEAK}
#  GROUPTREAT=() # rewrite for each group
#  GROUPCNTRL=() # also overwrite
#  # this reads all lines of $GROUP, not just current group ${GRPS[j]} lines
#  while read -r sample grouping condition; do 
#    # if this is the current group
#    if [ "$grouping" = "${GROUPNAME}" ]; then
#      # check if it is TREAT or CNTRL
#      if [ "$condition" = "TREAT" ]; then
#        # since group.txt file was created on $sample names, add $ALIGN to get OUT dir
#        GROUPTREAT+=("${sample}_${ALIGN}") # append the sample to group treatment array
#      elif [ "$condition" = "CNTRL" ]; then
#        GROUPCNTRL+=("${sample}_${ALIGN}") # append the sample to group control array
#      fi
#    fi
#    # check anywhere in $GROUP for global BCKGRND
#    if [ "$condition" = "BCKGRND" ]; then
#      GROUPCNTRL+=("${sample}_${ALIGN}") # append the sample to group control array
#    fi
#  done < "$GROUP"
#
#  # make a directory for grouped data
#  mkGRPDIR="mkdir -p $OUT/Group_${GRPS[j]}"
#  GRPOUT="$OUT/Group_${GRPS[j]}"
#  # not first use of MODE designation (all previous steps the same for all modes)
#  # also note different structure to args here. this allows us to pass an array
#  # of folders to -t without confusion
#  callPEAKS="./scripts/group/callpeaks.sh -m=$MODE -p=$PEAK -t=\"${GROUPTREAT[*]}\" \
#           -b=$GROUPCNTRL -d=$GRPOUT -i=$ID"
#
#  # grpCLEAN="./scripts/group/"
#
## these jobs are single threaded?
#  sed '
#s@__JOB_NAME__@'"$NAME"'@
#s@__THREADS__@4@
#s@__MEMFREE__@16@
#s/__EMAIL__/'"$EMAIL"'/
#s@__doMkOut__@'"$mkGRPDIR"'@
#s@__doCallPeaks__@'"$callPEAKS"'@
#' slurmGroup.template > ${ID}_slurm_script.sh
#  if [ "$GRPJOBIDS" == "" ]; then
#    # here we wait for JOBIDS thumbs up to run GRPJOBIDS
#    GRPJOBIDS=$(sbatch --dependency=afterok:${JOBIDS} ${ID}_slurm_script.sh | cut -d' ' -f4)
#  else
#    GRPJOBIDS=$GRPJOBIDS:$(sbatch --dependency=afterok:${JOBIDS} ${ID}_slurm_script.sh | cut -d' ' -f4)
#  fi
#  sleep 2
#done 
#
###
##
# Finally we run analyses which require all the data to be processed
##
###

# chkQUALITY="source /athena/hssgenomics/scratch/programs/MultiQC/bin/activate;\
#             multiqc $OUT; \
#             deactivate"

# cntPEAKREADS="Rscript --vanilla ${MCM} ${OUT}"

#doCOLLECT="mkdir -p $TOPDIR; mv ${OUT}/* ${TOPDIR}/"
#
#doCLEAN="ls ${TOPDIR}/*.sh | tar -czvf ${TOPDIR}/run_scripts.tar.gz -T -; \
#ls ${TOPDIR}/*.out | tar -czvf ${TOPDIR}/run_info.tar.gz -T -; \
#tar -czvf ${TOPDIR}/multiqc_data.tar.gz ${TOPDIR}/multiqc_data/; \
#rm $TOPDIR/*.sh; rm $TOPDIR/*.out; rm -r $TOPDIR/multiqc_data/"
## rm -rf $INDIRPATH/tmp
#
#sed '
#s@__JOB_NAME__@'"$NAME"'@
#s@__THREADS__@4@
#s@__MEMFREE__@16@
#s/__EMAIL__/'"$EMAIL"'/
#s@__doQuality__@'"$doQUALITY"'@
#s@__doCount__@'"$doCOUNT"'@
#s@__doCollect__@'"$doCOLLECT"'@
#s@__doClean__@'"$doCLEAN"'@
#' slurmGroup.template > ${GROUP}_slurm_script.sh 
#
#sbatch --dependency=afterok:${JOBIDS} ${NAME}_slurm_script.sh
#
# # here we submit one last job for experiment level analysis



exit 0
