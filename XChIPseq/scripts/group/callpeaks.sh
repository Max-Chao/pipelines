#!/bin/bash
# set -x

# Expected Input:
# # a single or group of BAM files for which peaks should be called 

# Expected Behavior:
# # call peaks for a single or group of BAMs with optional background BAM

# Expected output:
# # a bed file (.narrowPeak) and a signal file (.bdg)

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

# number of arugments $# is different for this script since -opt=arg is counted
# as a single value
if [ "$#" -lt 6 ]; then
  call "$0" "$@"
  usage
fi

# we need to pass multiple files to TREATMENT which means we need different syntax
# notice we don't use getopts for this script.
# also not the syntax of how variable is passed to TREATMENT -t="${VAR[*]}"
# and recieved here without quoting TREATMENT=(${i#*=})
for i in "$@"; do
  case $i in
    -m=*)
      MODE="${i#*=}"
      ;;
    -p=*)
      PEAK="${i#*=}"
      ;;
    -t=*) 
      TREATMENT=(${i#*=})
      ;;
    -b=*)
      BACKGROUND="${i#*=}"
      ;;
    -d=*)
      DIR="${i#*=}"
      ;;
    -i=*)
      ID="${i#*=}"
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

# incoming TREATMENT array is an array of group names
# that indicates where to find p and n sorted bam files...
if [ "$PEAK" = "macs2" ]; then
  # swap out treatment name for actual BAM files
  BAM=()
  for j in "${TREATMENT[@]}"; do
    # psorted for macs2
    BAM+=($(readlink -f $(find ./${j}/ -name "*.psorted.bam")))
  done
  BKG=$(readlink -f $(find ./${BACKGROUND}/ -name "*.psorted.bam"))
  # check first read of the first bam file for paired or single end
  # this checks the first 20ish reads for -f 1 (paired end flag)
  ISPE=$(samtools view -h ${BAM[0]} | head -n 50 | samtools view -c -f 1)
  if [ $ISPE -eq 1 ]; then
    ISPE="BAMPE"
  else
    ISPE="BAM"
  fi
  # estimate genome size fucking macs2 is a pain in my ass
  GENLEN=($(samtools view -H ${BAM[0]} | grep "^@SQ" - | cut -d':' -f3))
  GENLEN=$(echo ${GENLEN[@]} | tr ' ' '+' | bc)
  # source /opt/MACS2/bin/activate # for local testing
  source /athena/hssgenomics/scratch/programs/MACS2/bin/activate
elif [ "$PEAK" = "genrich" ]; then
  BAM=()
  for j in "${TREATMENT[@]}"; do
    # nsorted for genrich
    BAM+=($(find ./${j}/ -name "*.nsorted.bam"))
  done
  BKG=$(find ./${BACKGROUND}/ -name "*.nsorted.bam") 
fi

# fuck it I think we just have to do a bunch of if else here...F in the chat boys
if [ "$MODE" = "atac" ]; then
  if [ "$PEAK" = "genrich" ]; then
    if [ "$BACKGROUND" = "" ]; then
      # -j sets ATAC-seq mode for Genrich
      Genrich -y -j -d 100 -q 0.05 -t "${BAM[*]}" \
        -o $DIR/${ID}_genrich_treat_pileup.narrowPeak # \
#        -k $DIR/${ID}_genrich_peaks.bdg -f $DIR/${ID}.genrich.log
    else
      Genrich -y -j -d 100 -q 0.05 -t "${BAM[*]}" -c "$BKG" \
        -o $DIR/${ID}_genrich_treat_pileup.narrowPeak # \
#        -k $DIR/${ID}_genrich_peaks.bdg -f $DIR/${ID}.genrich.log
    fi
  elif [ $PEAK = "macs2" ]; then
    # this works even if $BACKGROUND is emtpy 
    # also note that MACS2 is actual trash and doesn't perform correctly for atacseq
    # this is simply here for backwards compatibility :/
    macs2 callpeak -f "$ISPE" --nomodel --shift -100 --extsize 200 -B -g "$GENLEN" \
      -q 0.05 -t "${BAM[@]}" --outdir $DIR -n $ID -c "$BKG" 
  else
    usage
fi 
elif [ "$MODE" = "chip" ]; then
  if [ "$PEAK" = "genrich" ]; then
    if [ "$BACKGROUND" = "" ]; then
      # -x expands single end read size equal to average read length
      Genrich -y -x -q 0.05 -t "${BAM[*]}" \
        -o $DIR/${ID}_genrich_treat_pileup.narrowPeak # \
#        -k $DIR/${ID}_genrich_peaks.bdg -f $DIR/${ID}.genrich.log
    else
      Genrich -y -x -q 0.05 -t "${BAM[*]}" -c "$BKG" \
        -o $DIR/${ID}_genrich_treat_pileup.narrowPeak # \
#        -k $DIR/${ID}_genrich_peaks.bdg -f $DIR/${ID}.genrich.log
    fi
  elif [ "$PEAK" = "macs2" ]; then
    macs2 callpeak -f "$ISPE" -B -g "$GENLEN" -q 0.05 -t "${BAM[@]}" \
      --outdir $DIR -n $ID -c "$BKG" 
  else
    usage
  fi 
elif [ "$MODE" = "cutNrun" ]; then
  # there is no reason to maintain backwards compatibility with macs
  # cut and run only works for broad peaks (not related to above comment)
  if [ "$BACKGROUND" = "" ]; then
    Genrich -y -x -q 0.05 -t "${BAM[*]}" \
      -o $DIR/${ID}_genrich_treat_pileup.narrowPeak # \
#      -k $DIR/${ID}_genrich_peaks.bdg -f $DIR/${ID}.genrich.log
  else
    Genrich -y -x -q 0.05 -t "${BAM[*]}" -c "$BKG" \
      -o $DIR/${ID}_genrich_treat_pileup.narrowPeak # \
      # -k $DIR/${ID}_genrich_peaks.bdg -f $DIR/${ID}.genrich.log
  fi
else
  usage
fi

# deactivate the virtual environment
if [ "$PEAK" = "macs2" ]; then
  deactivate
fi 

exit 0
