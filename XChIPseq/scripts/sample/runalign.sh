#!/bin/bash
# set -x

# Expected Input:
# # Fastq files that have been merged and (optionally) filtered and trimmed.
# # Either 1 (for single end) or 2 (for paired end) fastq files.

# Expected Behavior:
# # Incoming fastq files are aligned to the appropriate genome
# # using the requested aligner (bt2 or bwa)

# Expected output:
# # a BAM file of read alignments

# NOTE:
# # this script requires access to intermediate fastq files from 
# # several nodes. in order to do this...
# # DIR=$GLOBALTMP
# # OUT=$TMPDIR
# # so files are read in from OUT. split to DIR. aligned and written in DIR.
# # finally read from DIR and written to OUT by samtools merge.

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -ne 16 ]; then
  call "$0" "$@"
  usage
fi

while getopts :e:a:t:g:d:s:i:o: opt; do
  case $opt in
    e) 
      END=$OPTARG
      ;;
    a)
      ALIGNER=$OPTARG
      ;;
    t)
      THREADS=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

if [ "$END" == "pe" ]; then
  # step 1 split the fastq into equal sized chunks 4Gb each (~1Gb when zipped)
  # original files are in $TMPDIR (which is $OUT) and split files go to a 
  # globally visible tmp dir
  split -l 120000000 --additional-suffix=.fastq ${OUT}/${SAMPLE}.R1.merged.qc.fastq $DIR/${SAMPLE}.R1.merged.qc. 
  split -l 120000000 --additional-suffix=.fastq ${OUT}/${SAMPLE}.R2.merged.qc.fastq $DIR/${SAMPLE}.R2.merged.qc. 
  # this will fail if more than 26 files are generated
  READONEFQ=($(readlink -f $(find $DIR -name "${SAMPLE}.R1.merged.qc.a*.fastq" -print | sort)))
  READTWOFQ=($(readlink -f $(find $DIR -name "${SAMPLE}.R2.merged.qc.a*.fastq" -print | sort)))
  for (( i=0; i<${#READONEFQ[@]}; i++ )); do
    if [ "$ALIGNER" == "bt2" ]; then
      # this submits the job and stores the jobid for later
      STASH+=($(sbatch --job-name BowTie2_${i} -n1 -c6 --partition=panda --mem="16G" --wrap="bowtie2 --sensitive-local -q -p 6 -x $GENOME --no-unal --no-mixed --no-discordant --dovetail --phred33 -I 40 -X 700 -1 ${READONEFQ[$i]} -2 ${READTWOFQ[$i]} 2>$DIR/${SAMPLE}.${i}.bt2.log > $DIR/${SAMPLE}.${i}.bt2.sam" | cut -d' ' -f4))
    elif [ "$ALIGNER" == "bwa" ]; then
      STASH+=($(sbatch --job-name BWA_${i} -n1 -c12 --partition=panda --mem="16G" --wrap="bwa-mem2 mem -a -t 12 $GENOME $DIR/${SAMPLE}.R1.merged.qc.fastq $DIR/${SAMPLE}.R2.merged.qc.fastq 2>$DIR/${ID}.bwa.log > $DIR/${SAMPLE}.${i}.bwa.sam" | cut -d' ' -f4))
    fi
  done
elif [ "$END" == "se" ]; then
  split -l 120000000 --additional-suffix=.fastq $DIR/${SAMPLE}.merged.qc.fastq $DIR/${SAMPLE}.merged.qc. 
  READONEFQ=($(readlink -f $(find $DIR -name "${SAMPLE}.merged.qc.a*.fastq"))) 
  for (( i=0; i<${#READONEFQ[@]}; i++ )); do
    if [ "$ALIGNER" == "bt2" ]; then
      STASH+=($(sbatch --job-name BowTie2_${i} -n1 -c12 --partition=panda --mem="16G" --wrap="bowtie2 --sensitive-local -q -p 12 -x $GENOME --no-unal --phred33 -U $DIR/${SAMPLE}.merged.qc.fastq 2>$DIR/${ID}.bt2.log > $DIR/${SAMPLE}.${i}.bt2.sam" | cut -d' ' -f4))
    elif [ "$ALIGNER" == "bwa" ]; then
      STASH+=($(sbatch --job-name BWA_${i} -n1 -c12 --partition=panda --mem="16G" --wrap="bwa-mem2 mem -a -t 12 $GENOME $DIR/${SAMPLE}.merged.qc.fastq 2>$DIR/${ID}.bwa.log > $DIR/${SAMPLE}.${i}.bwa.sam" | cut -d' ' -f4))
    fi
  done
else
  usage
fi

# check if the jobs all completed...
STATUS="NOTDONE"
while [ "$STATUS" = "NOTDONE" ]; do  
  # if there are jobs in STASH
  if [ ${#STASH[@]} -gt 0 ]; then
    printf "%s Checking the status of JobID -- %s : Status -- %s\n" "$(date)" ${STASH[0]} $STATUS
    # if stash not empty, get job status for first job
    STASHSTATUS=$(sacct --format=state -n -P -j "${STASH[0]}" | sort | uniq)   
    # if the status is complete shift the array by 1
    if [ "$STASHSTATUS" = "COMPLETED" ]; then
      # shift the array (using unset will mess up indexing)
      STASH=("${STASH[@]:1}")
      # go back to the start (e.g. don't wait 5m to check the next id)
      continue
    # otherwise if the job failed ... exit
    elif [[ $STASHSTATUS =~ TIMEOUT|FAILED|CANCELLED ]]; then
      STATUS="FAIL"
    fi
  # when all jobs completed
  else
    STATUS="DONE"
    printf "%s Checking the status of JobID -- %s : Status -- %s\n" "$(date)" ${STASH[0]} $STATUS
  fi
  # check status every 300s (5min)
  sleep 300
done

# exit 
if [ "$STATUS" = "FAIL" ]; then
  printf "%s failed running jobid: %s.\n" $ALIGN ${STASH[0]}
  exit 1
fi

# now put the pieces back together
if [ "$END" = "pe" ]; then
  # all sam files in the $DIR need to be merged...and sent back to $OUT for rapid i/o
  # not that find executes merge with all files found and sends the results to stdout
  find $DIR/ -name "${SAMPLE}*.sam" -exec samtools merge --threads $THREADS - {} + | \
    samtools sort -n --threads $THREADS -T ${OUT}/${SAMPLE}.samtools.nsorting - | \
    samtools fixmate --threads $THREADS -rm - - | \
    samtools sort --threads $THREADS -T ${OUT}/${SAMPLE}.samtools.psorting - | \
    samtools markdup --threads $THREADS -rs - ${OUT}/${SAMPLE}.aligned.nodup.bam \
    2> $OUT/${ID}.markdup.log
elif [ "$END" = "se" ]; then
  find $DIR/ -name "${SAMPLE}*.sam" -exec samtools merge --threads $THREADS - {} + | \
    samtools sort --threads $THREADS - -o ${OUT}/${SAMPLE}.aligned.nodup.bam
fi

# collect log files and send them to $TMDIR
cat ${DIR}/${SAMPLE}*.log > ${OUT}/${ID}.${ALIGNER}.log

# should clear out $GLOBALTMP here cause it's gonna be big ...
# uncomment when done testing
# rm ${DIR}/${SAMPLE}*


exit 0
