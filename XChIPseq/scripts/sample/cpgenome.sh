#!/bin/bash
# set -x

# Expected Input:
# # NONE

# Expected Behavior:
# # this script takes as input the source and target directories where the 
# # genome index should be copied from and to. 

# Expected output:
# # NONE

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}


if [ "$#" -ne 4 ]; then
  call "$0" $"$@"
  usage
fi

while getopts :s:t: opt; do
  case $opt in
    s)
      SOURCE=$OPTARG
      ;;
    t)
      TARGET=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

# first make a directory in the tmpdir
mkdir -p $TARGET

# now pipe ls to xargs so that all files in the 
# source directory are copied in paralel 
# -I% tels xargs what character to replace with incoming
# data in the command call. e.g. rsync -p % <- is replaced with $SOURCE/*
# -P0 runs as many process as possible in parallel
ls $SOURCE/* | xargs -n1 -P0 -I% rsync -p % $TARGET

exit 0
