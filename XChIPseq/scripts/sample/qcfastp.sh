#!/bin/bash
# set -x

# Expected Input:
# # Merged Fastq files (possible filtered).
# # Either 1 (single end) or 2 (paired end) fastq files.

# Expected Behavior:
# # fastp 

# Expected output:
# # 1 (SE) or 2 (PE) fastq file ending in "*merged.qc.fastq"

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

# there are 5 options and each takes an arg so 1 parameter + 1 arg ... 5*2 = 10
if [ "$#" -ne 10 ]; then
  call "$0" "$@"
  usage
fi
    
while getopts :t:s:i:d:o: opt; do
  case $opt in
    t)
      THREADS=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

ISPE=$(find $DIR -name "${SAMPLE}*R2*" 2>/dev/null | wc -l)

if [ "$ISPE" -gt 0 ]; then 
  fastp -q 19 -n 20 -l 25 --thread $THREADS \
        -i ${DIR}/${SAMPLE}.R1.merged.fastq -I $DIR/${SAMPLE}.R2.merged.fastq \
        -o ${OUT}/${SAMPLE}.R1.merged.qc.fastq -O ${OUT}/${SAMPLE}.R2.merged.qc.fastq \
        --html ${OUT}/${ID}.fastp.html --json ${OUT}/${ID}.fastp.json 2>${OUT}/${ID}.fastp.log
else
  fastp -q 19 -n 20 -l 25 --thread $THREADS \
        -i $DIR/${SAMPLE}.merged.fastq -o ${OUT}/${SAMPLE}.merged.qc.fastq \
        --html ${OUT}/${ID}.fastp.html --json ${OUT}/${ID}.fastp.json 2>${OUT}/${ID}.fastp.log
fi

call "$0" "$@" >> ${OUT}/${ID}.call.log

exit 0      
