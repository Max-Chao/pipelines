#!/bin/bash
# set -x

# Expected Input:
# # a BAM file with duplicates, chrM, and black list regions removed
# # ending in *.aligned.nodup.noM.noblack.bam

# Expected Behavior:
# # generates stats, indexes, and multiple sortings
# # name sorted and position sorted

# Expected output:
# # name and position sorted BAMs with BAI indexes.

function usage {
  printf "Failed. Attempted to pass improper arguments to $(basename $0) \n"
  exit 22
}

function call {
  local script="$1"
  shift
  local arr=("$@")
  printf "$script "
  for i in "${arr[@]}"; do 
    printf "%s " $i 
  done
  printf "\n"
}

if [ "$#" -lt 12 ]; then
  call "$0" "$@"
  usage
fi

while getopts :t:s:i:p:d:o: opt; do
  case $opt in
    t)
      THREADS=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    i)
      ID=$OPTARG
      ;;
    p)
      TDIR=$OPTARG
      ;;
    d)
      DIR=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

# consider double checking parameters here.
# should all be checked by the parent script.

samtools sort -n --threads $THREADS -T ${TDIR}/${SAMPLE}.samtools.nsorted \
  ${DIR}/${SAMPLE}.aligned.nodup.noM.noblack.bam \
  -o ${OUT}/${ID}.aligned.nodup.noM.noblack.nsorted.bam
samtools sort --threads $THREADS -T ${TDIR}/${SAMPLE}.samtools.psorted \
  ${DIR}/${SAMPLE}.aligned.nodup.noM.noblack.bam \
  -o ${OUT}/${ID}.aligned.nodup.noM.noblack.psorted.bam

# samtools index $DIR/${ID}.aligned.nodup.noM.noblack.nsorted.bam
samtools index ${OUT}/${ID}.aligned.nodup.noM.noblack.psorted.bam

samtools stats --GC-depth 2e9 -m 0.999 -i 1000 --threads $THREADS \
  ${OUT}/${ID}.aligned.nodup.noM.noblack.psorted.bam > ${OUT}/${ID}.samStats.log

exit 0
