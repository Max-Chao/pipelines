#!/bin/bash -l 
#SBATCH --partition=panda 
#SBATCH --nodes=1 
#SBATCH --ntasks=1
#SBATCH --job-name=bulkRNA-Pipeline
#SBATCH --time=24:00:00  
#SBATCH --mem=2G
#SBATCH --mail-user=hssgenomics@gmail.com 
#SBATCH --mail-type=END,FAIL

# qsub scripts are executed in the CURRENT WORKING DIRECTORY

# General usage statement
usage() { 
  echo "Usage: qsub rnaseq_scheduler.sh in the directory where you want the results output.
  -i path to the project directory containing sample_ directories with fastq files (use full path).
  -o destination directory -- should be the corresponding project directory in the analysis branch.
  -n name -- sample name that matches all samples (e.g. Sample_KL).
  -g genome -- hg38, mm10 or rn6.
  -a annotation -- if hg38 or mm10 then gencode, otherwise ensembl
  -l aligner -- which aligner to use. Default = STAR
  -f filter -- does the fastq file contain reads that failed illumina chastity fitler?
  -r remove duplicates -- should PCR duplicates be removed (duplicates are always marked)
  -e email -- defaults to hssgenomics.at.gmail.dot.com
  -x exclude nodes -- default is none, nodes must be comma sep, i.e. node150,node160
  -h help -- prints this usage message." 
  exit 1
}

# Required variables
IN=""        # No default, script fails if full path not assigned
OUT=""       # Destination directory (should be corresponding analysis dir)
NAME=""      # The name common to all samples in the experiment
GENOME=""    # The genome build that that the samples were aligned to
ANNO=""      # default should probably be gencode...
ALIGN="STAR" # defualt aligner is STAR
PF="FALSE"   # does fastq include "Y" reads?
REMOVE="TRUE" # default to remove duplicates in paired-end data
EMAIL="hssgenomics@gmail.com"
EXCLUDE_NODES="NONE" # nodes to exclude

# Parse command-line arguments
while getopts :i:o:n:g:a:l:f:r:e:x:h: opt; do
  case $opt in
    i)
      IN=$OPTARG
      ;;
    o)
      OUT=$OPTARG
      ;;
    n)
      NAME=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    a)
      ANNO=$OPTARG
      ;;
    l)
      ALIGN=$OPTARG
      ;;
    f)
      PF=$OPTARG
      ;;
    r)
      REMOVE=$OPTARG
      ;;
    e)
      EMAIL=$OPTARG
      ;;
    x)
      EXCLUDE_NODES=$OPTARG
      ;;
    h)
      usage
      ;;
    *) 
      usage
      ;;
  esac
done

# set common variables (do not change with samples)
THREADS=12
MEMFREE=64
GENOMELOC=/athena/hssgenomics/scratch/genomes_2/${GENOME}/Genome/${ALIGN}/
ANNOLOC=/athena/hssgenomics/scratch/genomes_2/${GENOME}/Annotation/${ANNO}.latest.gtf

# Check validity of inputs
if [ ! -d "$IN" ] || [ "$IN" == "" ]; then
  printf "Invalid path to fastq directory. \n"
  usage
elif [ ! -d "$OUT" ] || [ "$OUT" == "" ]; then
  printf "Invalid output directory. \n"
  usage
elif [ "$(find "$IN" -maxdepth 1 -name "$NAME*" 2>/dev/null | wc -l)" == 0 ] || [ "$NAME" == "" ]; then
  printf "Invalid name argument. \n"
  usage
elif [ "$NAME" == "" ]; then
  printf "Invalid Sample ID. \n"
  usage
elif [[ ! $GENOME =~ hg38|mm10|rn6 ]] || [ ! -d "$GENOMELOC" ]; then
  printf "Invalid genome selected (only mm10, rn6, and hg38 are currently supported). \n"
  usage
elif [ "$ANNO" == "" ] || [ ! -f "$ANNOLOC" ]; then
  printf "Annotation not a file or is not specified. \n"
  usage
elif [[ ! $ALIGN =~ hisat2|STAR ]]; then
  printf "Incorrect aligner specified, only hisat2 and STAR supported. \n"
  usage
fi

# write a shell script with the command call
if [ $EXCLUDE_NODES == "NONE" ]; then
   printf "rnaseq_scheduler.sh -i $IN -o $OUT -n $NAME -g $GENOME -a $ANNO -l $ALIGN -f $PF -e $EMAIL\n" > $OUT/rnaseq_command.sh
else
   printf "rnaseq_scheduler.sh -i $IN -o $OUT -n $NAME -g $GENOME -a $ANNO -l $ALIGN -f $PF -e $EMAIL -n $EXCLUDE_NODES\n" > $OUT/rnaseq_command.sh 
fi

# copy the genome in parallel by listing the files and passing them to xargs and rsync
cpGENOME="mkdir \$TMPDIR/genomeIdx/; ls ${GENOMELOC}/* | xargs -n1 -P0 -I% rsync -p % \$TMPDIR/genomeIdx/"

# find the directories that match $NAME in the $IN directory, SAMPLDIRS is an array
mapfile -t SAMPLEDIRS < <(find "$IN" -type d -name "${NAME}*")

# for each directory found:
for (( i=0; i<${#SAMPLEDIRS[*]}; i++ )); do
  # get the sample name
  SAMPLENAME=$(basename "${SAMPLEDIRS[$i]}")
  # create output name
  ID=${SAMPLENAME}_${ALIGN}
  echo "$ID" # pring something so we can track it
  # if the current sample directory contains R2 reads then do paired end things
  # find regex attempts to match full path, this ls version is the result
  # ls doesn't understadn [0-9]{3} so [0-9][0-9][0-9] instead
  # NB: if Illumina decides that sample should exceed 2 digits this will fail
  if [ "$(ls ${SAMPLEDIRS[$i]}/*_L[0-9][0-9][0-9]_R2_[0-9][0-9][0-9].fastq.gz 2>/dev/null | wc -l)" -gt 0 ]; then
    # copy, unzip, and merge all at once
    cpFASTQ="zcat ${SAMPLEDIRS[$i]}/*_L[0-9][0-9][0-9]_R1_[0-9][0-9][0-9].fastq.gz > \$TMPDIR/${SAMPLENAME}.R1.merged; \
             zcat ${SAMPLEDIRS[$i]}/*_L[0-9][0-9][0-9]_R2_[0-9][0-9][0-9].fastq.gz > \$TMPDIR/${SAMPLENAME}.R2.merged"
    # run PF% filtering in parallel if reads are paired end
    if [ "$PF" == "TRUE" ]; then # pretty sure xargs is easier than this...
      pfFILTER="parallel -k -j 2 \"cat {1} | grep -A 3 '^@.* [^:]*:N:[^:]*:' | \
                egrep -v '^\-\-$' > {1.}.fastq\" ::: \$TMPDIR/*.merged.fastq" 
    else # if no filtering just rename the merged file back to fastq
      pfFILTER="rename .merged .merged.fastq \$TMPDIR/*.merged" 
    fi
    fqSCREEN="fastq_screen --outdir \$TMPDIR \$TMPDIR/${SAMPLENAME}.R1.merged.fastq \
                           \$TMPDIR/${SAMPLENAME}.R2.merged.fastq; \
              mv \$TMPDIR/${SAMPLENAME}.R1.merged_screen.txt \$TMPDIR/${ID}.R1_screen.txt; \
              mv \$TMPDIR/${SAMPLENAME}.R2.merged_screen.txt \$TMPDIR/${ID}.R2_screen.txt;"
    # run fastp and write files back out as merged.qc.fastq
    qcFASTP="fastp -q 19 -n 20 -l 20 --thread $THREADS \
             --detect_adapter_for_pe \
             -i \$TMPDIR/${SAMPLENAME}.R1.merged.fastq \
             -I \$TMPDIR/${SAMPLENAME}.R2.merged.fastq \
             -o \$TMPDIR/${SAMPLENAME}.R1.merged.qc.fastq \
             -O \$TMPDIR/${SAMPLENAME}.R2.merged.qc.fastq \
             --html \$TMPDIR/${ID}.fastp.html \
             --json \$TMPDIR/${ID}.fastp.json"
    # if aligner STAR 
    # set multimap to 4 since > 4 results in MAPQ < 1 ...i.e. 0
    if [ "$ALIGN" == "STAR" ]; then
      runALIGN="STAR --runThreadN $THREADS --runDirPerm All_RWX \
                --genomeDir \$TMPDIR/genomeIdx/ \
                --readFilesIn \$TMPDIR/${SAMPLENAME}.R1.merged.qc.fastq \
                              \$TMPDIR/${SAMPLENAME}.R2.merged.qc.fastq \
                --outFileNamePrefix \$TMPDIR/${ID}. \
                --outSAMtype BAM Unsorted \
                --outFilterMultimapNmax 4 \
                --outFilterType BySJout \
                --outSAMmultNmax 1 \
                --outFilterMismatchNoverReadLmax 0.1 \
                --outSAMstrandField intronMotif \
                --sjdbGTFfile $ANNOLOC \
                --outReadsUnmapped Fastx"
    else # else use HiSat2
      runALIGN="hisat2 --no-mixed --no-discordant --no-unal -p $THREADS -x \$TMPDIR/genomeIdx/genome \
                -1 \$TMPDIR/${SAMPLENAME}.R1.merged.qc.fastq \
                -2 \$TMPDIR/${SAMPLENAME}.R2.merged.qc.fastq | \
                samtools view --threads $THREADS -bS -o \$TMPDIR/${ID}.Aligned.out.bam"
    fi
    # filter out PCR duplicates
    if [ "$REMOVE" != "TRUE" ]; then
      runSORT="samtools sort --threads $THREADS -T \$TMPDIR -n \$TMPDIR/${ID}.Aligned.out.bam | \
               samtools fixmate --threads $THREADS -rm - - | \
               samtools sort --threads $THREADS -T \$TMPDIR - | \
               samtools markdup --threads $THREADS -l 800 -s -T \$TMPDIR - \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam"    
    else
      runSORT="samtools sort --threads $THREADS -T \$TMPDIR -n \$TMPDIR/${ID}.Aligned.out.bam | \
               samtools fixmate --threads $THREADS -rm - - | \
               samtools sort --threads $THREADS -T \$TMPDIR - | \
               samtools markdup --threads $THREADS -l 800 -rs -T \$TMPDIR - \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam"
    fi
    # need to count both STAR and hisat2 reads the same. 
    runCOUNT="featureCounts -p -a $ANNOLOC -s 0 -T 2 -o \$TMPDIR/unstranded.txt \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam; \
          featureCounts -p -a $ANNOLOC -s 1 -T 2 -o \$TMPDIR/forward.txt \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam; \
          featureCounts -p -a $ANNOLOC -s 2 -T 2 -o \$TMPDIR/reverse.txt \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam; \
          paste <(cut -f 1,7 \$TMPDIR/unstranded.txt) <(cut -f 7 \$TMPDIR/forward.txt) <(cut -f 7 \$TMPDIR/reverse.txt) | \
          column -s ' ' -t | tail -n +2 > \$TMPDIR/${ID}.ReadsPerGene.out.txt"
  # else if only R1 reads...
  else 
    # copy, unzip, merge...
    cpFASTQ="zcat ${SAMPLEDIRS[$i]}/*.fastq.gz > \$TMPDIR/${SAMPLENAME}.merged"
    # again check for filtering
    if [ "$PF" == "TRUE" ]; then # not done in parallel since it's a single file
      pfFILTER="cat \$TMPDIR/${SAMPLENAME}.merged | \
          grep -A 3 '^@.* [^:]*:N:[^:]*:' | \
          egrep -v '^\-\-$' > \$TMPDIR/${SAMPLENAME}.merged.fastq"
    else
      pfFILTER="rename .merged .merged.fastq \$TMPDIR/*.merged"
    fi
    fqSCREEN="fastq_screen --outdir \$TMPDIR \$TMPDIR/${SAMPLENAME}.merged.fastq; \
              mv \$TMPDIR/${SAMPLENAME}.merged_screen.txt \$TMPDIR/${ID}_screen.txt;"
    # run fastp and write passing reads to merged.qc.fastq
    qcFASTP="fastp -q 19 -n 20 -l 20 --thread $THREADS \
        -i \$TMPDIR/${SAMPLENAME}.merged.fastq \
        -o \$TMPDIR/${SAMPLENAME}.merged.qc.fastq \
        --json \$TMPDIR/${ID}.fastp.json \
        --html \$TMPDIR/${ID}.fastp.html"
    # if aligner STAR
    if [ "$ALIGN" == "STAR" ]; then 
      runALIGN="STAR --runThreadN $THREADS --runDirPerm All_RWX \
               --genomeDir \$TMPDIR/genomeIdx/ \
               --readFilesIn \$TMPDIR/${SAMPLENAME}.merged.qc.fastq \
               --outFileNamePrefix \$TMPDIR/${ID}. \
               --outSAMtype BAM SortedByCoordinate \
               --outFilterMultimapNmax 4 \
               --outFilterType BySJout \
               --outSAMmultNmax 1 \
               --outFilterMismatchNoverReadLmax 0.1 \
               --outSAMstrandField intronMotif \
               --sjdbGTFfile $ANNOLOC \
               --outReadsUnmapped Fastx"
      runSORT="echo Skipping sorting since data is not from paired-end samples."
#      runCOUNT="echo Skipping this step as well since we already counted with STAR"
      runCOUNT="featureCounts -a $ANNOLOC -s 0 -T 2 -o \$TMPDIR/unstranded.txt \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam; \
                featureCounts -a $ANNOLOC -s 1 -T 2 -o \$TMPDIR/forward.txt \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam; \
                featureCounts -a $ANNOLOC -s 2 -T 2 -o \$TMPDIR/reverse.txt \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam; \
                paste <(cut -f 1,7 \$TMPDIR/unstranded.txt) <(cut -f 7 \$TMPDIR/forward.txt) <(cut -f 7 \$TMPDIR/reverse.txt) | \
                column -s ' ' -t | tail -n +2 > \$TMPDIR/${ID}.ReadsPerGene.out.txt"
    else # use hiSat2
      runALIGN="hisat2 --no-unal -p $THREADS -x \$TMPDIR/genomeIdx/genome \
          -U \$TMPDIR/${SAMPLENAME}.merged.qc.fastq | \
          samtools sort --threads $THREADS -T \$TMPDIR -o \$TMPDIR/${ID}.Aligned.sortedByCoord.out.bam"
      runSORT="echo Skipping sorting since data is not from paired-end samples."
      runCOUNT="featureCounts -a $ANNOLOC -s 0 -T 2 -o \$TMPDIR/unstranded.txt ${ID}.Aligned.sortedByCoord.out.bam; \
          featureCounts -a $ANNOLOC -s 1 -T 2 -o \$TMPDIR/forward.txt ${ID}.Aligned.sortedByCoord.out.bam; \
          featureCounts -a $ANNOLOC -s 2 -T 2 -o \$TMPDIR/reverse.txt ${ID}.Aligned.sortedByCoord.out.bam; \
          paste <(cut -f 1,7 \$TMPDIR/unstranded.txt) <(cut -f 7 \$TMPDIR/forward.txt) <(cut -f 7 \$TMPDIR/reverse.txt) | \
          column -s ' ' -t | tail -n +2 > \$TMPDIR/${ID}.ReadsPerGene.out.txt"
    fi
  fi
  # copy the current fastq files to the $TMPDIR
  rmTEMP="ls \$TMPDIR/*; rm -r \$TMPDIR/${ID}._STARgenome/"
  mvDEST="mkdir -p $OUT/$ID; mv \$TMPDIR/${ID}.* ${OUT}/${ID}/"
  # build the each qsub script and submit
  sed '
s@__JOB_NAME__@'"$SAMPLENAME"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s/__EMAIL__/'"$EMAIL"'/
s@__cpGenome__@'"$cpGENOME"'@
s@__cpFastq__@'"$cpFASTQ"'@
s@__pfFilter__@'"$pfFILTER"'@
s@__fqScreen__@'"$fqSCREEN"'@
s@__qcFastp__@'"$qcFASTP"'@
s@__runAlign__@'"$runALIGN"'@
s@__runSort__@'"$runSORT"'@
s@__runCount__@'"$runCOUNT"'@
s@__rmTemp__@'"$rmTEMP"'@
s@__mvDest__@'"$mvDEST"'@
' slurmAligner.template > ${ID}_slurm_script.sh 
  sleep 2  # let it breath
  # qsub the script
  if [ "$JOBIDS" == "" ]
  then
     if [ $EXCLUDE_NODES != "NONE" ]; then
        JOBIDS=$(sbatch --exclude=$EXCLUDE_NODES ${ID}_slurm_script.sh | cut -d' ' -f4) 
        # Submitted batch job ->JOBID<-
     else
        JOBIDS=$(sbatch ${ID}_slurm_script.sh | cut -d' ' -f4) # Submitted batch job ->JOBID<-
     fi
  else
     if [ $EXCLUDE_NODES != "NONE" ]; then
        JOBIDS=$JOBIDS:$(sbatch --exclude=$EXCLUDE_NODES ${ID}_slurm_script.sh | cut -d' ' -f4)
     else
        JOBIDS=$JOBIDS:$(sbatch ${ID}_slurm_script.sh | cut -d' ' -f4)
     fi
  fi
done

# Start Summarization
MCM=/athena/hssgenomics/scratch/programs/detools/summarizeCounts.r
TOPDIR=$OUT/$NAME

doQUALITY="source /athena/hssgenomics/scratch/programs/MultiQC/bin/activate; \
multiqc $OUT; \
deactivate"

doCOUNT="Rscript --vanilla $MCM $OUT"

doLIST="ls $OUT/*"

doCOLLECT="mkdir -p $TOPDIR; mv $OUT/* $TOPDIR/"

doCLEAN="cd $TOPDIR; ls ${NAME}*.sh | tar -czvf run_scripts.tar.gz -T -; \
ls *.out | tar -czvf run_info.tar.gz -T -; \
tar -czvf multiqc_data.tar.gz multiqc_data/; \
rm *.sh; rm *.out; rm -r multiqc_data/"

sed '
s@__JOB_NAME__@'"$NAME"'@
s@__THREADS__@4@
s@__MEMFREE__@16@
s/__EMAIL__/'"$EMAIL"'/
s@__doQuality__@'"$doQUALITY"'@
s@__doCount__@'"$doCOUNT"'@
s@__doList__@'"$doLIST"'@
s@__doCollect__@'"$doCOLLECT"'@
s@__doClean__@'"$doCLEAN"'@
' slurmSummary.template > ${NAME}_slurm_script.sh 

if [ $EXCLUDE_NODES != "NONE" ]; then
  sbatch --exclude=$EXCLUDE_NODES --dependency=afterok:${JOBIDS} ${NAME}_slurm_script.sh
else
   sbatch --dependency=afterok:${JOBIDS} ${NAME}_slurm_script.sh
fi

exit 0
