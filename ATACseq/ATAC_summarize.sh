#!/bin/bash -l
#SBATCH --partition=panda
#SBATCH --ntasks=1 --cpus-per-task=4
#SBATCH --job-name=$2"."summary
#SBATCH --time=144:00:00   # HH/MM/SS
#SBATCH --mem=16GB
#SBATCH --mail-user=hssgenomics@gmail.com
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

err() {
  echo "$1...exiting"
  exit 1 # any non-0 exit code signals an error";
}

# function to check return code of programs.";
# exits with standard message if code is non-zero;";
# otherwise displays completiong message and date.";
#   arg1 is the return code (usually $?)";
#   arg2 is text describing what ran";
ckRes() {
  if [ "$1" == "0" ]; then
    echo "..Done $2 `date`"
  else
    err "$2 returned non-0 exit code $1"
  fi
}

usage() {
  echo "Usage: sbatch this script (CNR_summarize.sh) in the directory where you want the results ouptut to with the following args.
  -d path to the sample directory (use full path to the directory)
  -p peakcaller --user defined peak caller, MACS2 (default), GENRICH or SEACR
  -h help -- prints this usage message" 
}

#
# Variables
#
INDIRPATH=""       # Default dir is the current working directory
CALLER="MACS2"     # eventually alternate peak callers could be handled...

#
# Parse command-line arguments
#
while getopts :d:p:h opt; do
  case $opt in
    d)
      INDIRPATH=$OPTARG
      ;;
    p)
      CALLER=$OPTARG
      ;;
    h)
      usage; exit 0
      ;;
  esac
done

# set variables and load packages
TOPDIR=$INDIRPATH/
PEAKSDIR=$TOPDIR/PEAKS
BAMSDIR=$TOPDIR/BAMS
TRACKSDIR=$TOPDIR/TRACKS
SAMSTATDIR=$TOPDIR/SAMStat
LOGSDIR=$TOPDIR/LOGS
 
#    (1) generate GTF from peaks
#    (2) count reads in peaks
cd $PEAKSDIR
cat $(ls *.narrowPeak) | cut -f 1,2,3,4 | sort -k 1,1 -k 2,2n | \
bedtools merge -d 10 -c 4 -o collapse -i - | cut -d ',' -f 1 | \
bedToGenePred stdin stdout | genePredToGtf file stdin stdout | \
tr '\000' '.' > allPeaks.union.gtf
ckRes $? "Generating peaks GTF."

BAMFILES=$(find $TOPDIR -name *.bam | tr '\n' ' ')
time featureCounts -p -T 4 -t exon -g gene_id -a allPeaks.union.gtf -o ${NAME}.counts.txt $BAMFILES
ckRes $? "Counting reads per feature."
cd $TOPDIR

#
# Step 2: run multiqc
#
source /athena/hssgenomics/scratch/programs/MultiQC/bin/activate
multiqc $TOPDIR # waiting for spack version
ckRes $? "Running MultiQC."
deactivate

# Organize
find $TOPDIR -name '*.samStats.txt' -exec mv -t $SAMSTATDIR/ {} +
find $TOPDIR -name '*.bam*' -exec mv -t $BAMSDIR/ {} +
find $TOPDIR -name '*.bw' -exec mv -t $TRACKSDIR/ {} +
zip slurm_outs.zip slurm-*; rm slurm-*; # remove the leftover scripts
zip shell_scripts.zip *.sh *.pl; rm *.sh *.pl;
mv *_OUT $LOGSDIR; mv multiqc_data $LOGSDIR; mv slurm_outs.zip $LOGSDIR; mv shell_scripts.zip $LOGSDIR

echo "Summarization completed without errors. Exiting..."
exit 0
