#! /bin/bash 

#
# Variables
#
OUTDIRPATH=$(pwd)
INDIRPATH="" 			# The directory that contains the Sample folders with fastq files
THREADS=12
MEMORY="64G"
GENOME=""			# The genome build. E.g., hg19, hg18, mm9, mm10
EMAIL="hssgenomics@gmail.com"
PEAKCALL="MACS2"
FILTER="FALSE"
usage() 
{ 
  echo "Usage: ATAC_pipeline_PE.sh run this in the Analysis dir where you want the output
  -i inputdir -- path to directory containing the Fastq data with Sample dirs
  -o outdir -- path to the output directory (use full path)
  -s sample sheet -- a two column file specifying samples and controls respectively (no headers) 
  -g genome -- hg38/mm10/rn6
  -e email -- your non-hss email address
  -p peakcaller MACS2, Genrich  default: MACS2
  -f whether to perform filtering of illumina chastity reads. default: FALSE"
}

#
# Parse command-line arguments
#
while getopts :i:o:s:g:e:p:f: opt; do
  case $opt in
    i)
      INDIRPATH=$OPTARG
      ;;
    o)
      OUTDIRPATH=$OPTARG
      ;;
    s)
      SAMSHT=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    e)
      EMAIL=$OPTARG
      ;;
    p)
      PEAKCALL=$OPTARG
      ;;
    f)
      FILTER=$OPTARG
      ;;
    *) 
      usage; exit 1
      ;;
  esac
done

#
# Check input arguments
#
if [ "$INDIRPATH" == "" ] || [ ! -d $INDIRPATH ]; then
  echo "Need to give a readable input directory. Exiting..."
  usage
  exit 1
elif [ "$OUTDIRPATH" == "" ] || [ ! -d $OUTDIRPATH ]; then
  echo "Output dir not a readable path. Exiting..."
  usage
  exit 1
elif [ "$GENOME" == "" ]; then
  echo "Need to give genome information. Exiting..."
  usage
  exit 1
elif [ "$SAMSHT" == "" ]; then
  echo "Sample sheet must be specified. Exiting..."
  usage
  exit 1
elif [[ ! $FILTER =~ true|True|TRUE|false|False|FALSE ]]; then
  echo "-f option must specify true or false. Exiting..."
  usage
  exit 1
fi

#
# Each set of sample FASTQ files aligned independently  
#
for i in $(ls -d ${INDIRPATH}/Sample_*/); do
  samplename=`basename $i`
  echo $samplename
  if [ "$(ls ${INDIRPATH}/${samplename}/*_L[0-9][0-9][0-9]_R2_[0-9][0-9][0-9].fastq.gz 2>/dev/null | wc -l)" -gt 0 ]; then
    TYPE="Paired"
  else 
    TYPE="Single"
  fi
  ./ATAC_alignment.pl --dir=$i --out=$OUTDIRPATH --n_threads=$THREADS --memory=$MEMORY \
--genome=$GENOME --email=$EMAIL --se_pe=$TYPE --filter=$FILTER > ATAC_align_$samplename.sh
  chmod +x ATAC_align_$samplename.sh
  #sbatch ATAC_alignment_PE_$samplename.sh
  if [ "$JOBIDS" == "" ]; then
    JOBIDS=$(sbatch ATAC_align_$samplename.sh | cut -d ' ' -f4)
  else
    JOBIDS=$JOBIDS:$(sbatch ATAC_align_$samplename.sh | cut -d ' ' -f4)
  fi
done

#
# Peak calling occurs on pairs of samples
#
# # # instead of writing out a bunch of files, just dump the commands?
while read sample control; do
  printf "%s\n" "sbatch ATAC_callpeaks.sh -d $OUTDIRPATH -g $GENOME -s $sample -r $TYPE -e $EMAIL -p $PEAKCALL" > ATAC_peakcall_${sample}.sh
  chmod a+x ATAC_peakcall_${sample}.sh
  if [ "$JID" == "" ]; then
    JID=$(sbatch --dependency=afterok:${JOBIDS} ATAC_callpeaks.sh -d $OUTDIRPATH -g $GENOME -s $sample -c -r $TYPE -e $EMAIL -p $PEAKCALL | cut -d ' ' -f4)
  else
    JID=$JID:$(sbatch --dependency=afterok:${JOBIDS} ATAC_callpeaks.sh -d $OUTDIRPATH -e $EMAIL -p $PEAKCALL -g $GENOME -s $sample -r $TYPE | cut -d ' ' -f4)
  fi
done < $SAMSHT

#
# Finish the summarization process
#
printf "%s\n" "sbatch ATAC_summarize.sh -d $OUTDIRPATH -p $PEAKCALL" > ATAC_summary.sh
chmod a+x ATAC_summary.sh
sbatch --dependency=afterok:${JID} ATAC_summarize.sh -d $OUTDIRPATH -p $PEAKCALL

exit 0
