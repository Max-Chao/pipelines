#!/usr/bin/perl -l
use strict;
use warnings;
use Getopt::Long;
use File::Spec;
use File::Basename;
use Cwd;

# Set definite variables
my $bin           = dirname(File::Spec->rel2abs(__FILE__));
# my $resultsdir    = cwd();
my @names         = ();        # Stores the whole directory path       

# Variables holding paths 
my $genome_dir        = "/athena/hssgenomics/scratch/genomes_2/";

# Variables inherited from command line
my $dir       = undef;	# Input directory
my $out       = cwd();
my $n_threads = 12;     # Number of available threads
my $memory    = "64G";	# Maximum memory per job (-m)
my $genome    = undef;	# Genome build? (-g_
my $email     = undef;	# Default email
my $filter    = undef;
my $se_pe     = undef;

# Placeholding variables
my $name    = undef;    # Stores the basename of the directory for output file names
my $cmd1    = undef;    # Rsync genome.fa to tmp
my $cmd2_1  = undef;    # Remove reads failing to pass illumina filter
my $cmd2_2  = undef;    # Run trim_galore command
my $cmd3_1  = undef;    # trim, qc, align, filter, sort (by name), fixmate
my $cmd3_2  = undef;    # re-sort (by coordinates), markdup
my $cmd3_3  = undef;    # 
my $cmd4_1  = undef;    # Samtools filtering
my $cmd4_2  = undef;    # adjust tn5 offsets
my $cmd4_3  = undef;    # Samtools sort
my $cmd4_4  = undef;    # samtools index
my $cmd4_5  = undef;    # Remove chrM
my $cmd4_6  = undef;    # Remove blacklist
my $cmd5_1  = undef;    # Split reads command
my $cmd5_2  = undef;    # Estimate number of clonal reads
my $cmd5_3  = undef;    # Make reads track for UCSC Genome Browser

if (@ARGV == 0) {
    die "Lack of arguments. Run script as: CNR_pipeline.sh\n
    --dir       = Input directory.
    --out       = Output directory.
    --n_threads	= Number of available threads. Default is 12.
    --memory    = Maximum memory available. Default is 48.
    --genome    = Genome build, e.g., hg38, mm10
    --se_pe     = Single end or paired end reads
    --filter    = filter illumina chastity reads
    --email     = Email to send job alerts.";
}

# Read options
GetOptions(
"dir=s"       => \$dir,
"out=s"       => \$out,
"n_threads=s" => \$n_threads,
"memory=s"    => \$memory,
"genome=s"    => \$genome,
"se_pe=s"     => \$se_pe,
"filter=s"    => \$filter,
"email=s"     => \$email);

# Store prefix for output files
@names          = split('/',$dir);
$name           = $names[-1];
$out    .= "/".$name."_OUT";

# Create results directory
unless(mkdir $out) {
    die "Unable to create $out\n";
}

# Select appropriate genome sequence
my $genome_index    = "$genome_dir/$genome/Genome/bt2/";
my $blacklist       = "$genome_dir/$genome/Annotation/blacklist.bed";

# ---------------
# start bash file for slurm
# ---------------
print "#!/bin/bash -l";
print "#SBATCH --partition=panda";
print "#SBATCH --ntasks=1 --cpus-per-task=$n_threads";
print "#SBATCH --job-name=$name"."CNR_aln";
print "#SBATCH --time=72:00:00";   # HH/MM/SS
print "#SBATCH --mem=$memory";
print "#SBATCH --mail-user=$email";
print "#SBATCH --mail-type=END,FAIL";
# new requirement for SLURM
print "source ~/.bashrc";
print "set +x";

	
print "# general function that exits after printing its text argument";
print "# in a standard format which can be easily grep'd.";
print "err() {";
print "  echo \"\$1...exiting\"";
print "  exit 1 # any non-0 exit code signals an error";
print "}";
print "\n";
print "# function to check return code of programs.";
print "# exits with standard message if code is non-zero;";
print "# otherwise displays completiong message and date.";
print "#   arg1 is the return code (usually $?)";
print "#   arg2 is text describing what ran";
print "ckRes() {";
print "  if [ \"\$1\" == \"0\" ]; then";
print "    echo \"..Done \$2 `date`\"";
print "	 else";
print "	   err \"\$2 returned non-0 exit code \$1\"";
print "	 fi";
print "}";
print "\n\n";

# GCC9.2.0
# print "spack load parallel/y2wn5ch; spack load zlib\@1.2.11/d3u7hmw;";
# print "spack load samtools/nh5s3fh; spack load bowtie2/khacf4c";
# GCC8.2.0
print "spack load parallel/y2wn5ch; spack load zlib/d3u7hmw;";
print "spack load samtools/3jymtx6; spack load bowtie2/ztcq4ql;";

#
# Step 1: (1) Copy genome.fa, (2) genome index, and (3) sample fastq to current tmp dir
#
$cmd1    = "rsync -rLp $genome_index/* \$TMPDIR";
print "echo STEP 1.2: Copy Aligner Indexes to TMP Directory \n";
print "time $cmd1 \n";
print "ckRes \$? \"rsync Aligner Indexes to TMP Directory\";";

$genome_index    = "\$TMPDIR/$genome";

#
# Step 2: Preprocessing
# (1) merge all reads in sample
# (2) remove platform failed reads
#
if( $se_pe eq "Paired" ){
  $cmd2_1    = "cat $dir/*R1*.fastq.gz > \$TMPDIR/$name.R1.merged.gz && cat $dir/*R2*.fastq.gz > \$TMPDIR/$name.R2.merged.gz";
  print "echo STEP 2.1: Merging and Moving R1 and R2 Fastq Files \n";
  print "time $cmd2_1 \n";
  print "ckRes \$? \"Merging and Moving R1 and R2 Fastq Files\";";
} elsif( $se_pe eq "Single" ) {
  $cmd2_1    = "cat $dir/*R1*.fastq.gz > \$TMPDIR/$name.R1.merged.gz";
  print "echo STEP 2.1: Merging and Moving Fastq Files \n";
  print "time $cmd2_1 \n";
  print "ckRes \$? \"Merging and Moving Fastq Files\";";
} else {
  # this should never be able to occur since parameter is set in pipeline script
  exit(1)
}

if ( $filter =~ /^true$/i ) {
  $cmd2_2    = "parallel -k -j 2 \"zcat {1} | grep -A 3 '^@.* [^:]*:N:[^:]*:' | egrep -v '^\\-\\-\$\' > {1.}.fastq\" ::: \$TMPDIR/*.merged.gz";
  print "echo STEP 2.2: Removing Illumina Failed Reads";
  print "time $cmd2_2 \n";
  print "ckRes \$? \"Removing Illumina Failed Reads\";";
} elsif ( $filter =~ /^false$/i ){
  $cmd2_2    = "parallel -k -j 2 \"zcat {1} > {1.}.fastq\" ::: \$TMPDIR/*.merged.gz";
  print "echo STEP 2.2: Decompressing Reads";
  print "time $cmd2_2 \n";
  print "ckRes \$? \"Decompressing Reads\";";
} else {
  exit(1)
}

# Step 3: Trimming, Aligning, Filtering
# (1) fastp to trim and QC | bowtie2 to align | samtools filter and sort and fixmate
# (2) samtools remove duplicates 
# (3) generate index
# NB: --no-discordant flag added due to false peaks e.g. --> chr2:11,155,095-11,155,423 <--
if( $se_pe eq "Paired" ){
  $cmd3_1    = "fastp -q 20 -n 10 -l 20 --thread $n_threads --in1 \$TMPDIR/$name.R1.merged.fastq --in2 \$TMPDIR/$name.R2.merged.fastq --html \$TMPDIR/$name.fastp.html --json \$TMPDIR/$name.fastp.json --stdout | bowtie2 --very-sensitive-local --no-discordant --dovetail -X 1000 -N 1 -q -p $n_threads -x $genome_index --interleaved - 2>\$TMPDIR/$name.bowtie2.txt | samtools view -uhb -q 2 -F 2828 -@ $n_threads - | samtools sort -n -@ $n_threads - | samtools fixmate -@ $n_threads -rm - \$TMPDIR/$name.fixmate.bam";
  print "echo STEP 3.1: Trimming, QC, Aligning, Filtering, Sorting by Name, Fixing Mates";
  print "time $cmd3_1 \n";
  print "ckRes \$? \"Trimming, QC, Aligning, Filtering, Sorting by Name, Fixing Mates\";";
} elsif ( $se_pe eq "Single" ) {
  # keep output name as fixmate.bam but no mates need fixing..just convenience
  $cmd3_1    = "fastp -q 20 -n 10 -l 20 --thread $n_threads -i \$TMPDIR/$name.R1.merged.fastq --html \$TMPDIR/$name.fastp.html --json \$TMPDIR/$name.fastp.json --stdout | bowtie2 --very-sensitive-local -N 1 -q -p $n_threads -x $genome_index -U - 2>\$TMPDIR/$name.bowtie2.txt | samtools view -uhb -q 2 -F 2820 -@ $n_threads - -o \$TMPDIR/$name.fixmate.bam";
  print "echo STEP 3.1: Trimming, QC, Aligning, and Filtering";
  print "time $cmd3_1 \n";
  print "ckRes \$? \"Trimming, QC, Aligning, and Filtering\";";
} else {
  exit(1)
}

$cmd3_2    = "samtools sort -@ $n_threads \$TMPDIR/$name.fixmate.bam | samtools markdup -@ $n_threads -s - \$TMPDIR/$name.aligned.markdup.bam";
print "echo STEP 3.2: Re-Sorting by Coordinates and marking Duplicates";
print "time $cmd3_2 \n";
print "ckRes \$? \"Re-Sorting by Coordinates and marking Duplicates\";";

$cmd3_3    = "samtools index \$TMPDIR/$name.aligned.markdup.bam";
print "echo STEP 3.3: Samtools Index \n";
print "time $cmd3_3 \n";
print "ckRes \$? \"Samtools Index\";";
#
# Step 4: Post Processing
# (1)  a. get chrM locations b. remove chrM reads
# (2) Adjust for Tn5 offset
# (3) Re-sort
# (4) Remove Black Sites
# (5) Index
# (6) Generate Fragment Size Distribution
#
$cmd4_1   = "samtools idxstats \$TMPDIR/$name.aligned.markdup.bam > \$TMPDIR/$name.alignment_per_chromosome.txt";
print "echo STEP 4.1: Creating Samtools Index\n";
print "time $cmd4_1 \n";
print "ckRes \$? \"Creating Samtools Index\";";

$cmd4_2   = "cat \$TMPDIR/$name.alignment_per_chromosome.txt | cut -f 1 | grep -v chrM | xargs samtools view -hb \$TMPDIR/$name.aligned.markdup.bam > \$TMPDIR/$name.aligned.markdup.noM.bam";
print "echo STEP 4.2: Removing chrM \n";
print "time $cmd4_2 \n";
print "ckRes \$? \"Removing chrM\";";

$cmd4_3    = "samtools sort -@ $n_threads -o \$TMPDIR/$name.aligned.markdup.noM.sorted.bam \$TMPDIR/$name.aligned.markdup.noM.bam";
print "echo STEP 4.3: Resort Reads with Samtools\n";
print "time $cmd4_3 \n";
print "ckRes \$? \"Resort Reads with Samtools\";";

# $cmd4_4    = "bedtools subtract -A -abam \$TMPDIR/$name.aligned.markdup.noM.sorted.bam -b $blacklist > \$TMPDIR/$name.aligned.markdup.noM.sorted.noblack.bam";
# print "echo STEP 4.4: Bedtools Remove Black Listed Regions \n";
# print "time $cmd4_4 \n";
# print "ckRes \$? \"Bedtools Remove Black Listed Regions\";";

$cmd4_5    = "samtools index \$TMPDIR/$name.aligned.markdup.noM.sorted.bam";
print "echo STEP 4.5: Samtools Regenerate Index \n";
print "time $cmd4_5 \n";
print "ckRes \$? \"Samtools Regenerate Index\";";

$cmd4_6    = "samtools stats -m 1 -i 1000 -@ $n_threads \$TMPDIR/$name.aligned.markdup.noM.sorted.bam | grep ^IS | cut -f 2- > \$TMPDIR/$name.samStats.txt";
print "echo STEP 4.6: Samtools Fragment Length Distribution \n";
print "time $cmd4_6 \n";
print "ckRes \$? \"Samtools Fragment Length Distribution\";";
#
# Step 5: Copy Files to Results Directory
#
$cmd5_1        ="ls \$TMPDIR/*";
print "echo STEP 5.1: Checking \$TMPDIR Contents \n";
print "time $cmd5_1 \n";
print "ckRes \$? \"Checking \$TMPDIR Contents\";";

$cmd5_2        ="cp -v \$TMPDIR/*.txt $out && cp -v \$TMPDIR/*fastp* $out";
print "echo STEP 5.2: Copy Log Files to $out \n";
print "time $cmd5_2 \n";
print "ckRes \$? \"Copy Log Files to $out\";";

$cmd5_3        ="cp -v \$TMPDIR/$name.aligned.markdup.noM.sorted.bam* $out ";
print "echo STEP 5.3: Copy bam files to $out \n";
print "time $cmd5_3 \n";
print "ckRes \$? \"Copy Bam Files and Unaligned Reads to $out\";";

print "echo Pipeline completed without errors. Exiting..."; 
print "exit 0";
