#! /bin/bash 

#
# Variables
#
INDIRPATH="" 			# The directory that contains the Sample folders with fastq files
THREADS=8
MEMORY="50G"
GENOME=""			# The genome build. E.g., hg19, hg18, mm9, mm10
ALIGNER="bt2"
EMAIL="doliv071@gmail.com"

usage() 
{ 
  echo "Usage: chipseq_pipeline_SE.sh 
  -i inputdir -- path to directory containing the data
  -t threads -- number of threads (8 default)
  -m memory -- amount of total memory (50G default) 
  -g genome -- hg19/hg38/mm9/mm10
  -a aligner -- bt2 or bwa aligner
  -e email -- your non-hss email address"
  exit 1 
}

#
# Parse command-line arguments
#
while getopts :i:t:m:g:a:e: opt; do
  case $opt in
    i)
      INDIRPATH=$OPTARG
      ;;
    t)
      THREADS=$OPTARG
      ;;
    m)
      MEMORY=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    a)
      ALIGNER=$OPTARG
      ;;
#    e)
#      EMAIL=$OPTARG
#      ;;
    *) 
      usage
      ;;
  esac
done

#
# Check input arguments
#
if [ "$INDIRPATH" == "" ] || [ ! -d $INDIRPATH ]; then
  echo "Need to give a readable input directory. Exiting..."
  usage 
elif [ "$GENOME" == "" ]; then
  echo "Need to give genome information. Exiting..."
  usage
elif [[ ! $ALIGNER =~ bt2|bwa ]]; then
  echo "Please provide proper aligner name (either bt2 or bwa). Exiting..."
  usage
fi

#
# Generate and submit scripts 
#  --email=$EMAIL
for i in `ls -d ${INDIRPATH}/Sample_*/`; do
  samplename=`basename $i`
  echo $samplename
   /athena/hssgenomics/scratch/pipelines/ChIPseq/chipseq_alignment_SE.pl --dir=$i --n_threads=$THREADS --memory_pt=$MEMORY --genome=$GENOME --aligner=$ALIGNER > a_alignment_SE_$samplename.sh
  chmod +x a_alignment_SE_$samplename.sh
  sbatch a_alignment_SE_$samplename.sh
done
