#!/bin/bash -l
#SBATCH --partition=panda
#SBATCH --ntasks=1 --cpus-per-task=12
#SBATCH --job-name=$2"."peakCall
#SBATCH --time=144:00:00   # HH/MM/SS
#SBATCH --mem=64GB
#SBATCH --mail-user=hssgenomics@gmail.com
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

set -x

#####################################################################
# Summarization script for aggregating all the results from a single 
# C&R experiment and generating summary stats including 
# QC (multiqc), peak calling, overlay tracks, and sample correlation
#####################################################################
#
# Functions
#
err() {
  echo "$1...exiting"
  exit 1 # any non-0 exit code signals an error";
}

# function to check return code of programs.";
# exits with standard message if code is non-zero;";
# otherwise displays completiong message and date.";
#   arg1 is the return code (usually $?)";
#   arg2 is text describing what ran";
ckRes() {
  if [ "$1" == "0" ]; then
    echo "..Done $2 `date`"
  else
    err "$2 returned non-0 exit code $1"
  fi
}

# General usage statement
usage() { 
  echo "Usage: sbatch this script (CNR_callpeaks.sh) in the directory where you want the results ouptut to with the following args.
  -d path to the sample directory (use full path to the directory)
  -s sample to call peaks on
  -c control (input) sample to call peaks against
  -r read type (paired or single end)
  -g genome build -- hg19, hg38 (default), mm9, mm10
  -e email -- user's email (do not use hss.edu)
  -p peakcaller --user defined peak caller, MACS2 (default), GENRICH or SEACR
  -h help -- prints this usage message" 
}

#
# Variables
#
INDIRPATH=""       # Default dir is the current working directory
SAMPLE=""	   # sample (first column of sample sheet)
CONTROL=""         # input control (second column of sample sheet)
READTYPE=""
GENOME=""          # The genome build that that the samples were aligned to
EMAIL="hssgenomics@gmail.com"
CALLER="MACS2"
#
# Parse command-line arguments
#
while getopts :d:s:c:r:g:e:p:h opt; do
  case $opt in
    d)
      INDIRPATH=$OPTARG
      ;;
    s)
      SAMPLE=$OPTARG
      ;;
    c)
      CONTROL=$OPTARG
      ;;
    r)
      READTYPE=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    e)
      EMAIL=$OPTARG
      ;;
    p)
      CALLER=$OPTARG
      ;;
    h)
      usage; exit 0
      ;;
    *) 
      usage; exit 1
      ;;
  esac
done

#
# Check validity of inputs
#
if [ ! -d $INDIRPATH ] || [ "$INDIRPATH" == "" ]; then
  echo "Invalid input directory. \n"
  usage
  exit 1
elif [ "$SAMPLE" == "" ]; then
  echo "Sample is missing with no default"
  usage
  exit 1
elif [ "$CONTROL" == "" ]; then
  echo "Control missing with no default"
  usage
  exit 1
elif [[ ! $GENOME =~ rn6|hg38|mm10 ]]; then
  echo "Invalid genome build selected. \n"
  usage
  exit 1
elif [ "$EMAIL" == "" ]; then
  echo "Invalid email address, please enter a valid email. \n"
  usage
  exit 1
elif [[ ! $CALLER =~ MACS2|GENRICH|SEACR ]]; then
  echo "Invalid peak caller selected. \n"
  usage
  exit 1
elif [ "$READTYPE" == "" ]; then
  echo "Invalid read type selected."
  usage
  exit 1
fi

# set variables and load packages
TOPDIR=$INDIRPATH/
PEAKSDIR=$TOPDIR/PEAKS
BAMSDIR=$TOPDIR/BAMS
TRACKSDIR=$TOPDIR/TRACKS
SAMSTATDIR=$TOPDIR/SAMStat
LOGSDIR=$TOPDIR/LOGS

GENOMEPATH=/athena/hssgenomics/scratch/genomes_2/$GENOME
# CHRSIZE=$GENOMEPATH/Genome/chrom.sizes
GENOMEMAP=$GENOMEPATH/Genome/chrom.data
GENOMESIZE=$(awk '{ map = $2 * $3 ; tot += map ; print tot }' $GENOMEMAP | tail -n 1)
GENRICH="/athena/hssgenomics/scratch/programs/Genrich/Genrich"
# peak calling functions --- no long saves bedgraph ---
if [ $CALLER == "MACS2" ]; then 
  if [ "$READTYPE" == "Paired" ]; then 
    macs2CallPeaks(){
      time macs2 callpeak -f BAMPE --nomodel --shift -37 --extsize 76 -g $GENOMESIZE -q 0.05 -t $1 -c $2 -n $3
    }
  else
    macs2CallPeaks(){
      time macs2 callpeak -f BAM --nomodel --shift -37 --extsize 76 -g $GENOMESIZE -q 0.05 -t $1 -c $2 -n $3 
    }
  fi  
elif [ $CALLER == "GENRICH" ]; then
  genrichCallPeaksNoInput(){
    time Genrich -t $1 -o ${2}_${1}.narrowPeak
  }
  genrichCallPeakswithInput(){
    time Genrich -t $1 -c $CONTROL -o ${2}_${1}.narrowPeak
  }
elif [ $CALLER == "SEACR" ]; then
  seacrcallpeaks(){
    time SEACR 
  }
fi


#
# Step 1: aggregate samples for summarization
#

# TOCOPY=$(find $INDIRPATH -maxdepth 1 -name "$NAME*")
# echo "creating $TOPDIR and moving $TOCOPY files there..."
# mkdir $TOPDIR; # create the summary directory 
# mv $TOCOPY $TOPDIR; # move the sample directories there

# tarball and move pipeline scripts and logs
# cd $INDIRPATH; zip run_scripts.zip *.sh; 
# mv run_scripts.zip $TOPDIR;
# rm *.sh; # remove the leftover scripts
# make some directories for outputs
mkdir $PEAKSDIR $BAMSDIR $TRACKSDIR $SAMSTATDIR $LOGSDIR

#
# Step 2: Call peaks with macs2
#

# MACS2, python3.8, numpy 
spack load py-macs2/iu6dcyy
spack load python/coxzyuo
spack load py-numpy/uk742f5

cd $PEAKSDIR
echo "Begin Calling peaks...`date`"
time macs2CallPeaks ${TOPDIR}/${SAMPLE}_OUT/*.aligned.markdup.noM.sorted.bam ${TOPDIR}/${CONTROL}_OUT/*.aligned.markdup.noM.sorted.bam $SAMPLE 2> ${SAMPLE}.log.txt
ckRes $? "Calling Peaks"
cd $TOPDIR # always return to top so we don't get lost
# MUST UNLOAD FOR virtual environments TO WORK
spack unload py-macs2/iu6dcyy
spack unload python/coxzyuo
spack unload py-numpy/uk742f5

#
# Step 3 convert bdg to bw
#

# the binSize and smoothLength parameters might need to be exposed to user?
# bigwigs are just visual candy.
cd $TRACKSDIR
printf '%s\n' "chrM" > ign_chr.txt
printf '%s: Begin generating sample bigwig \n' "`date`"
# note that binsize and smoothen attempt to make tracks look like what MACS2 sees when calling peaks
time singularity exec /athena/hssgenomics/scratch/programs/BAMscale.sif BAMscale scale \
--bam ${TOPDIR}/${SAMPLE}_OUT/*.aligned.markdup.noM.sorted.bam --binsize 25 --smoothen 1 \
--normtype base --scale genome --blacklist ign_chr.txt --threads 12
ckRes $? "Generating Sample BigWigs"
printf '%s: Begin generating control bigwig \n' "`date`"
# note that binsize and smoothen attempt to make tracks look like what MACS2 sees when calling peaks
time singularity exec /athena/hssgenomics/scratch/programs/BAMscale.sif BAMscale scale \
--bam ${TOPDIR}/${CONTROL}_OUT/*.aligned.markdup.noM.sorted.bam --binsize 25 --smoothen 1 \
--normtype base --scale genome --blacklist ign_chr.txt --threads 12
ckRes $? "Generating Control BigWigs"
cd $TOPDIR

echo "Peak calling completed without errors."
exit 0
