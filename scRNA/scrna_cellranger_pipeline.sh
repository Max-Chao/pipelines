#!/bin/bash -l 
#SBATCH --partition=panda 
#SBATCH --nodes=1 
#SBATCH --ntasks=1
#SBATCH --job-name=scRNA-Pipeline
#SBATCH --time=24:00:00  
#SBATCH --mem=2G
#SBATCH --mail-user=hssgenomics@gmail.com 
#SBATCH --mail-type=END,FAIL

source ~/.bash_profile

# this scipt is a test template for spinning up jobs and waiting for them
# to complete before moving on. 
# In other words, if a script needs to do something to all samples in a set
# we can we throw these jobs to the server queue and wait for them to complete
# before moving on. 
# This would solve our pipelining issue of having a Perl script that writes
# a bash script for each sample. And then a separate summarize script for
# when all those piplines complete.

err() {
  echo "$1...exiting"
  exit 1
}
ckRes() {
  if [ "$1" == "0" ]; then
    echo "..Done $2 `date`"
  else
    err "$2 returned non-0 exit code $1"
  fi
}

# General usage statement
usage() { 
  echo "Usage: qsub this script (scrna_cellranger_pipeline.sh) in the directory where you want the results output.
  -f path to the project directory containing sample_ directories with fastq files (use full path).
  -d destination directory -- should be the corresponding project directory in the analysis branch.
  -n name -- sample name that matches all samples (e.g. Sample_KL).
  -g genome -- hg38 or mm10 (only mm10 and hg38 are supported by cell_ranger).
  -h help -- prints this usage message." 
  exit 1
}

#
# Variables
#
FASTQPATH=""  # No default, script fails if full path not assigned
DESTDIR=""    # Destination directory (should be corresponding analysis dir)
NAME=""       # The name common to all samples in the experiment
GENOME=""     # The genome build that that the samples were aligned to

#
# Parse command-line arguments
#
while getopts :f:d:n:g:h: opt; do
  case $opt in
    f)
      FASTQPATH=$OPTARG
      ;;
    d)
      DESTDIR=$OPTARG
      ;;
    n)
      NAME=$OPTARG
      ;;
    g)
      GENOME=$OPTARG
      ;;
    h)
      usage
      ;;
    *) 
      usage
      ;;
  esac
done


#
# Check validity of inputs
#
if [ ! -d $FASTQPATH ] || [ "$FASTQPATH" == "" ]; then
  echo "Invalid path to fastq directory. \n"
  usage
elif [ `find $FASTQPATH -maxdepth 1 -name "$NAME*" 2>/dev/null | wc -l` == 0 ] || [ "$NAME" == "" ]; then
  echo "Invalid name argument. \n"
  usage
elif [[ ! $GENOME =~ hg38|mm10 ]]; then
  echo "Invalid genome selected (only mm10 and hg38 are valid for cell_ranger). \n"
  usage
elif [ "$NAME" == "" ]; then
  echo "Invalid Sample ID. \n"
  usage
fi

# set variables and load packages
GENOMEPATH=/athena/hssgenomics/scratch/genomes_2/$GENOME/Genome/CellRanger/refdata-cellranger*
SLURMHEADER=/athena/hssgenomics/scratch/pipelines/scRNA/slurm.header

# set variables for this qsub loop (this shouldn't need to be changed by user
# so we don't make these global options
THREADS=16
MEMFREE=64

echo "library_id,molecule_h5" > aggr.csv
SAMPLEDIRS=(`find $FASTQPATH -type d -name "${NAME}*"`)
for (( i=0; i<${#SAMPLEDIRS[*]}; i++ )); do
  SLURMNAME=`basename ${SAMPLEDIRS[$i]}`
  ID=${SLURMNAME}_CellRanger
  FASTQNAME=`ls ${SAMPLEDIRS[$i]} | grep -P -o '.+?(?=_S[0-9]{1,2}_L[0-9]{3})' | uniq`
  ### Build Job Commands
  TOCPCMD="cp -r $GENOMEPATH \$TMPDIR; cp -r ${SAMPLEDIRS[$i]} \$TMPDIR"
  SLURMCMD="cd \$TMPDIR; time cellranger count --disable-ui --jobmode=local --localmem=$MEMFREE --localcores=$THREADS --id=$ID --transcriptome=\$TMPDIR/`basename $GENOMEPATH` --fastqs=\$TMPDIR/${SLURMNAME} --sample=$FASTQNAME"
  MVCMD="mkdir -p $DESTDIR; mv \$TMPDIR/$ID $DESTDIR"
  # build the each qsub script and submit
  sed '
s@__JOB_NAME__@'"$SLURMNAME"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s@__TOCPCMD__@'"$TOCPCMD"'@
s@__SLURMCMD__@'"$SLURMCMD"'@
s@__MVCMD__@'"$MVCMD"'@  
' $SLURMHEADER > ${SLURMNAME}_slurm_script.sh 
  # chmod a+x ${SLURMNAME}_slurm_script.sh
  # qsub the script
  if [ "$JOBIDS" == "" ]; then
    JOBIDS=`sbatch ${SLURMNAME}_slurm_script.sh | cut -d' ' -f4` # Submitted batch job ->JOBID<-
  else
    JOBIDS=$JOBIDS:`sbatch ${SLURMNAME}_slurm_script.sh | cut -d' ' -f4`
  fi
  echo "$ID,$DESTDIR/$ID/outs/molecule_info.h5" >> aggr.csv
done

AGGRCMD="cellranger aggr --disable-ui --jobmode=local --localmem=$MEMFREE --localcores=$THREADS --id=$NAME --csv=aggr.csv --normalize=none"
SLURMNAME=${NAME}_Aggregate
sed '
s@__JOB_NAME__@'"$SLURMNAME"'@
s@__THREADS__@'"$THREADS"'@
s@__MEMFREE__@'"$MEMFREE"'@
s@__TOCPCMD__@@
s@__SLURMCMD__@'"$AGGRCMD"'@
s@__MVCMD__@@
' $SLURMHEADER > ${SLURMNAME}_slurm_script.sh 

sbatch --dependency=afterok:${JOBIDS} ${SLURMNAME}_slurm_script.sh

exit 0

