#!/bin/bash
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:CNR CNR_alignment.pl | tar xf - > CNR_alignment.pl
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:CNR CNR_callpeaks.sh | tar xf - > CNR_callpeaks.sh
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:CNR CNR_summarize.sh | tar xf - > CNR_summarize.sh
git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git HEAD:CNR CNR_pipeline.sh | tar xf - > CNR_pipeline.sh
if [ "$1" == "with-tests" ]; then
  git archive --format=tar --remote=git@gitlab.com:hssgenomics/pipelines.git \
      HEAD:CNR test/ | tar xf - 
fi
chmod a+x CNR_alignment.pl CNR_callpeaks.sh CNR_summarize.sh CNR_pipeline.sh

exit 0 
