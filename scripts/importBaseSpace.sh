#!/bin/bash

# variables
PROJID=""
DIR=""
# need project ID
usage() 
{ 
  echo "load_bs_proj.sh
  please run this in the fastq directory for the PI
  -d directory to create
  -p project id number"
  exit 1 
}
while getopts :d:p: opt; do
  case $opt in
    d)
      DIR=$OPTARG
      ;;      
    p)
      PROJID=$OPTARG
      ;;
    *) 
      usage
      ;;
  esac
done

if [ "$PROJID" == "" ]; then
        echo "Need to give a real project ID to download the project. 
        Exiting..."
        bs -c hssgenomics list project
        usage
elif [ "$DIR" == "" ]; then
	echo "Need to give a readable input directory to place projects. Exiting..."
  	usage
fi

mkdir $DIR
chmod +rwx $DIR
cd $DIR
# download project
bs -c hssgenomics download project -i $PROJID -o .
echo "fully downloaded"
# clean up names
for i in *_ds.*; do mv -n ${i} Sample_${i%%\_ds.*}; done

# check if there are more than 1 lane
a=(*_L00*)
names=()
for i in ${!a[@]}; do
	names+=(${a[i]\%%*_L00*})
done
# we now have an array with like names -> remove duplicate names and create these directories
UNIQ_IDS=($(printf "%s\n" "${names[@]}" | sort -u))

# check if UNIQ_IDs already exist
# if not, create new dir
for i in ${!UNIQ_IDS[@]}; do
	if [ ! -d ${UNIQ_IDS[i]} ]; then
		mkdir ${UNIQ_IDS[i]}
	fi
done

# mv all files into directory
# check if array length of names and uniq IDs are not equal. 
# if not equal, mv fastq's to newly generated names

if [ ${#names[*]} != ${#UNIQ_IDS} ]; then
	for i in ${!a[@]}; do
		mv ${a[i]}/* ${names[i]}
		rmdir ${a[i]}
	done
fi	

# merge fastqs within the new dirs based on R1 R2
for i in Sample_*; do
	if ls ${i}/*_R2_*.fastq.gz 1> /dev/null 2>&1; then
		cat ${i}/*_R1_* > ${i}_R1.fastq.gz
		cat ${i}/*_R2_* > ${i}_R2.fastq.gz
		rm ${i}/*
		mv ${i}_R1.fastq.gz ${i}_R2.fastq.gz ${i}		
	 else
       		cat ${i}/* > ${i}_R1.fastq.gz
		rm ${i}/*
		mv ${i}_R1.fastq.gz ${i}
		
	fi
done

