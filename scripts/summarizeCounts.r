 
args <- commandArgs(trailingOnly=TRUE)

topdir <- args[1] #$TOPDIR

# find out the strandedness of the data, requires an input file with 3 columns of counts
# where 1st is sum of 2 and 3, 2nd is forward strand counts, and 3rd is reverse strand counts
whichStrand <- function(file){
    df <- read.delim(file = file, header = FALSE, row.names = 1, skip = 1, stringsAsFactors = FALSE) 
    df <- df[grep("^N_", rownames(df), invert = T),] # remove bad STAR features
    # if forward strand has less than half as many reads as the reverse strand then
    # use the reverse strand
    if(colSums(df)[2]*2 < colSums(df)[3]){
        df <- df[, 3, drop = FALSE]
    } else if(colSums(df)[3]*2 < colSums(df)[2]) { # otherwise if 2xreverse less than forward use forward
        df <- df[, 2, drop = FALSE]
    } else { # otherwise it is an unstranded library
        df <- df[, 1, drop = FALSE]
    }
    colnames(df) <- strsplit(file, split="[/]|[.]")[[1]][1]
    colnames(df) <- gsub("_STAR|_hisat2", "", colnames(df))   
    return(df)
}

# # once we have strand information, extract the appropriate column
# tableProcessing <- function(file){
#     df <- read.delim(file = file, header = FALSE, row.names = 1, skip = 4, stringsAsFactors = FALSE) 
#     switch(strand,
#            "reverse" = {df <- df[, 3, drop = FALSE]},
#            "forward" = {df <- df[, 2, drop = FALSE]},
#            "none"    = {df <- df[, 1, drop = FALSE]}
#     )
#     colnames(df) <- strsplit(file, split="[/]|[.]")[[1]][1]
#     colnames(df) <- gsub("STAR|hisat2", "", colnames(df))   
#     return(df)
# }

#list of all ReadperGene files located in topdir (and subdir's)
countFiles <- list.files(path = topdir, pattern = "ReadsPerGene.out.tab", full.names = F, recursive = TRUE)  
countMatrix <- cbind.data.frame(lapply(countFiles, whichStrand))
write.table(countMatrix, file = paste0(topdir, "/countMatrix.tsv"), sep = "\t", row.names = TRUE, col.names = NA)

